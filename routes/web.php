<?php

Auth::routes();

Route::group(['prefix' => 'backend/', 'as'  =>  'backend.', 'namespace' =>  'Backend\\','middleware' => 'auth'], function (){

    //Dashboard
    Route::get('dashboard',                             ['as' => 'dashboard',                           'uses' => 'DashboardController@index']);

    //Route of setting
    Route::get('setting',                                  ['as' => 'setting',                                'uses' => 'SettingController@index']);
    Route::get('setting/create',                           ['as' => 'setting.create',                         'uses' => 'SettingController@create']);
    Route::post('setting/store',                            ['as' => 'setting.store',                          'uses' => 'SettingController@store']);
    Route::get('setting/{id}/edit',                        ['as' => 'setting.edit',                           'uses' => 'SettingController@edit']);
    Route::get('setting/{id}',                        ['as' => 'setting.show',                           'uses' => 'SettingController@show']);
    Route::post('setting/{id}/update',                      ['as' => 'setting.update',                         'uses' => 'SettingController@update']);
    Route::get('setting/{id}/delete',                      ['as' => 'setting.delete',                         'uses' => 'SettingController@delete']);

    //Route of page
    Route::get('page',                                  ['as' => 'page',                                'uses' => 'PageController@index']);
    Route::get('page/create',                           ['as' => 'page.create',                         'uses' => 'PageController@create']);
    Route::post('page/store',                            ['as' => 'page.store',                          'uses' => 'PageController@store']);
    Route::get('page/{id}/edit',                        ['as' => 'page.edit',                           'uses' => 'PageController@edit']);
    Route::get('page/{id}',                        ['as' => 'page.show',                           'uses' => 'PageController@show']);
    Route::post('page/{id}/update',                      ['as' => 'page.update',                         'uses' => 'PageController@update']);
    Route::get('page/{id}/delete',                      ['as' => 'page.delete',                         'uses' => 'PageController@delete']);

    //Route of slider
    Route::get('slider',                                  ['as' => 'slider',                                'uses' => 'SliderController@index']);
    Route::get('slider/create',                           ['as' => 'slider.create',                         'uses' => 'SliderController@create']);
    Route::post('slider/store',                            ['as' => 'slider.store',                          'uses' => 'SliderController@store']);
    Route::get('slider/{id}/edit',                        ['as' => 'slider.edit',                           'uses' => 'SliderController@edit']);
    Route::get('slider/{id}',                        ['as' => 'slider.show',                           'uses' => 'SliderController@show']);
    Route::post('slider/{id}/update',                      ['as' => 'slider.update',                         'uses' => 'SliderController@update']);
    Route::get('slider/{id}/delete',                      ['as' => 'slider.delete',                         'uses' => 'SliderController@delete']);

    //Route of service
    Route::get('service',                                  ['as' => 'service',                                'uses' => 'ServiceController@index']);
    Route::get('service/create',                           ['as' => 'service.create',                         'uses' => 'ServiceController@create']);
    Route::post('service/store',                            ['as' => 'service.store',                          'uses' => 'ServiceController@store']);
    Route::get('service/{id}/edit',                        ['as' => 'service.edit',                           'uses' => 'ServiceController@edit']);
    Route::get('service/{id}',                        ['as' => 'service.show',                           'uses' => 'ServiceController@show']);
    Route::post('service/{id}/update',                      ['as' => 'service.update',                         'uses' => 'ServiceController@update']);
    Route::get('service/{id}/delete',                      ['as' => 'service.delete',                         'uses' => 'ServiceController@delete']);

    //Route of course
    Route::get('course',                                  ['as' => 'course',                                'uses' => 'CourseController@index']);
    Route::get('course/create',                           ['as' => 'course.create',                         'uses' => 'CourseController@create']);
    Route::post('course/store',                            ['as' => 'course.store',                          'uses' => 'CourseController@store']);
    Route::get('course/{id}/edit',                        ['as' => 'course.edit',                           'uses' => 'CourseController@edit']);
    Route::get('course/{id}',                        ['as' => 'course.show',                           'uses' => 'CourseController@show']);
    Route::post('course/{id}/update',                      ['as' => 'course.update',                         'uses' => 'CourseController@update']);
    Route::get('course/{id}/delete',                      ['as' => 'course.delete',                         'uses' => 'CourseController@delete']);


    //Route of ward
//    Route::get('ward',                                  ['as' => 'ward',                                'uses' => 'WardController@index']);
//    Route::get('ward/create',                           ['as' => 'ward.create',                         'uses' => 'WardController@create']);
//    Route::post('ward/store',                            ['as' => 'ward.store',                          'uses' => 'WardController@store']);
//    Route::get('ward/{id}/edit',                        ['as' => 'ward.edit',                           'uses' => 'WardController@edit']);
//    Route::get('ward/{id}',                        ['as' => 'ward.show',                           'uses' => 'WardController@show']);
//    Route::post('ward/{id}/update',                      ['as' => 'ward.update',                         'uses' => 'WardController@update']);
//    Route::get('ward/{id}/delete',                      ['as' => 'ward.delete',                         'uses' => 'WardController@delete']);

    //Route of Post
    Route::get('category',                             ['as' => 'category',                            'uses' => 'CategoryController@index']);
    Route::get('category/create',                      ['as' => 'category.create',                     'uses' => 'CategoryController@create']);
    Route::post('category/store',                       ['as' => 'category.store',                      'uses' => 'CategoryController@store']);
    Route::get('category/{id}/edit',                   ['as' => 'category.edit',                       'uses' => 'CategoryController@edit']);
    Route::get('category/{id}',                        ['as' => 'category.show',                           'uses' => 'CategoryController@show']);
    Route::get('category/{id}/view',                   ['as' => 'category.view',                       'uses' => 'CategoryController@view']);
    Route::post('category/{id}/update',                 ['as' => 'category.update',                     'uses' => 'CategoryController@update']);
    Route::get('category/{id}/delete',                 ['as' => 'category.delete',                     'uses' => 'CategoryController@delete']);

    //Route of Contact Us
    Route::get('contactus',                             ['as' => 'contactus',                            'uses' => 'ContactUsController@index']);
//    Route::get('contactus/create',                      ['as' => 'contactus.create',                     'uses' => 'ContactUsController@create']);
    Route::post('contactus/store',                       ['as' => 'contactus.store',                      'uses' => 'ContactUsController@store']);
    Route::get('contactus/{id}/edit',                   ['as' => 'contactus.edit',                       'uses' => 'ContactUsController@edit']);
    Route::get('contactus/{id}',                        ['as' => 'contactus.show',                           'uses' => 'ContactUsController@show']);
    Route::get('contactus/{id}/view',                   ['as' => 'contactus.view',                       'uses' => 'ContactUsController@view']);
    Route::post('contactus/{id}/update',                 ['as' => 'contactus.update',                     'uses' => 'ContactUsController@update']);
    Route::get('contactus/{id}/delete',                 ['as' => 'contactus.delete',                     'uses' => 'ContactUsController@delete']);
    Route::get('contactus/{id}/reply',                 ['as' => 'contactus.reply',                     'uses' => 'ContactUsController@reply']);

    //Route of News
    Route::get('news',                             ['as' => 'news',                            'uses' => 'NewsController@index']);
    Route::get('news/create',                      ['as' => 'news.create',                     'uses' => 'NewsController@create']);
    Route::post('news/store',                       ['as' => 'news.store',                      'uses' => 'NewsController@store']);
    Route::get('news/{id}/edit',                   ['as' => 'news.edit',                       'uses' => 'NewsController@edit']);
    Route::get('news/{id}',                        ['as' => 'news.show',                           'uses' => 'NewsController@show']);
    Route::get('news/{id}/view',                   ['as' => 'news.view',                       'uses' => 'NewsController@view']);
    Route::post('news/{id}/update',                 ['as' => 'news.update',                     'uses' => 'NewsController@update']);
    Route::get('news/{id}/delete',                 ['as' => 'news.delete',                     'uses' => 'NewsController@delete']);

    //Route of Client
    Route::get('client',                             ['as' => 'client',                            'uses' => 'ClientController@index']);
    Route::get('client/create',                      ['as' => 'client.create',                     'uses' => 'ClientController@create']);
    Route::post('client/store',                       ['as' => 'client.store',                      'uses' => 'ClientController@store']);
    Route::get('client/{id}/edit',                   ['as' => 'client.edit',                       'uses' => 'ClientController@edit']);
    Route::get('client/{id}',                        ['as' => 'client.show',                           'uses' => 'ClientController@show']);
    Route::get('client/{id}/view',                   ['as' => 'client.view',                       'uses' => 'ClientController@view']);
    Route::post('client/{id}/update',                 ['as' => 'client.update',                     'uses' => 'ClientController@update']);
    Route::get('client/{id}/delete',                 ['as' => 'client.delete',                     'uses' => 'ClientController@delete']);

    //Route of Testimonial
    Route::get('testimonial',                             ['as' => 'testimonial',                            'uses' => 'TestimonialController@index']);
    Route::get('testimonial/create',                      ['as' => 'testimonial.create',                     'uses' => 'TestimonialController@create']);
    Route::post('testimonial/store',                       ['as' => 'testimonial.store',                      'uses' => 'TestimonialController@store']);
    Route::get('testimonial/{id}/edit',                   ['as' => 'testimonial.edit',                       'uses' => 'TestimonialController@edit']);
    Route::get('testimonial/{id}',                        ['as' => 'testimonial.show',                           'uses' => 'TestimonialController@show']);
    Route::get('testimonial/{id}/view',                   ['as' => 'testimonial.view',                       'uses' => 'TestimonialController@view']);
    Route::post('testimonial/{id}/update',                 ['as' => 'testimonial.update',                     'uses' => 'TestimonialController@update']);
    Route::get('testimonial/{id}/delete',                 ['as' => 'testimonial.delete',                     'uses' => 'TestimonialController@delete']);


    //Route of Advertiser
//    Route::get('advertiser',                             ['as' => 'advertiser',                            'uses' => 'AdvertiserController@index']);
//    Route::get('advertiser/create',                      ['as' => 'advertiser.create',                     'uses' => 'AdvertiserController@create']);
//    Route::post('advertiser/store',                      ['as' => 'advertiser.store',                      'uses' => 'AdvertiserController@store']);
//    Route::get('advertiser/{id}/edit',                   ['as' => 'advertiser.edit',                       'uses' => 'AdvertiserController@edit']);
//    Route::get('advertiser/{id}/view',                   ['as' => 'advertiser.view',                       'uses' => 'AdvertiserController@view']);
//    Route::post('advertiser/{id}/update',                ['as' => 'advertiser.update',                     'uses' => 'AdvertiserController@update']);
//    Route::get('advertiser/{id}/delete',                 ['as' => 'advertiser.delete',                     'uses' => 'AdvertiserController@delete']);

    //Route of Video
//    Route::get('video',                             ['as' => 'video',                            'uses' => 'VideoController@index']);
//    Route::get('video/create',                      ['as' => 'video.create',                     'uses' => 'VideoController@create']);
//    Route::post('video/store',                      ['as' => 'video.store',                      'uses' => 'VideoController@store']);
//    Route::get('video/{id}/edit',                   ['as' => 'video.edit',                       'uses' => 'VideoController@edit']);
//    Route::get('video/{id}/view',                   ['as' => 'video.view',                       'uses' => 'VideoController@view']);
//    Route::post('video/{id}/update',                ['as' => 'video.update',                     'uses' => 'VideoController@update']);
//    Route::get('video/{id}/delete',                 ['as' => 'video.delete',                     'uses' => 'VideoController@delete']);

    //Route of Reporter
//    Route::get('reporter',                             ['as' => 'reporter',                            'uses' => 'ReporterController@index']);
//    Route::get('reporter/create',                      ['as' => 'reporter.create',                     'uses' => 'ReporterController@create']);
//    Route::post('reporter/store',                      ['as' => 'reporter.store',                      'uses' => 'ReporterController@store']);
//    Route::get('reporter/{id}/edit',                   ['as' => 'reporter.edit',                       'uses' => 'ReporterController@edit']);
//    Route::get('reporter/{id}/view',                   ['as' => 'reporter.view',                       'uses' => 'ReporterController@view']);
//    Route::post('reporter/{id}/update',                ['as' => 'reporter.update',                     'uses' => 'ReporterController@update']);
//    Route::get('reporter/{id}/delete',                 ['as' => 'reporter.delete',                     'uses' => 'ReporterController@delete']);

});


Route::get('/',                                ['as' => 'home',                                  'uses' => 'Frontend\HomeController@index']);
Route::get('/aboutus',                         ['as' => 'aboutus',                                'uses' => 'Frontend\AboutusController@index']);
Route::get('/service',                         ['as' => 'service',                                'uses' => 'Frontend\ServiceController@index']);
Route::get('/service/{id}',                     ['as' => 'service.show',                           'uses' => 'Frontend\ServiceController@show']);
Route::get('/course',                         ['as' => 'course',                                'uses' => 'Frontend\CourseController@index']);
Route::get('/course/{id}',                     ['as' => 'course.show',                           'uses' => 'Frontend\CourseController@show']);

Route::get('/testimonial',                     ['as' => 'testimonial',                            'uses' => 'Frontend\TestimonialController@index']);
Route::get('/news',                            ['as' => 'news',                                   'uses' => 'Frontend\NewsController@index']);
Route::get('/news/{id}',                     ['as' => 'news.show',                           'uses' => 'Frontend\NewsController@show']);
Route::get('/contactus',                       ['as' => 'contactus',                               'uses' => 'Frontend\ContactusController@index']);
Route::post('/contactus',                       ['as' => 'contactus.store',                      'uses' => 'Frontend\ContactusController@store']);


Route::get('/category/{category}',             ['as' => 'category_list',                         'uses' => 'Frontend\CategoryController@index']);
Route::get('/details/{slug}',                  ['as' => 'article.details',                       'uses' => 'Frontend\NewsController@index']);
Route::get('/video',                           ['as' => 'video',                                 'uses' => 'Frontend\HomeController@video']);


