jQuery(document).ready(function () {
    /** *****************************************
     * Preloader
     * */
    var preloader_wrapper = jQuery('#preloader-wrapper'),
        preloader = jQuery('.preloader');

    if (preloader_wrapper.length > 0 && preloader.length > 0) {
        preloader.addClass('loading');

        function preloaderTimeFunction() {
            setTimeout(function () {
                preloader_wrapper.fadeOut('slow').removeClass("loading");
                preloader.fadeOut('slow').removeClass("loading");
            }, 1000);
        }

        jQuery(window).on('load', function (e) {
            sessionStorage.loaded = "yes";
            preloaderTimeFunction();
        });

        if (sessionStorage.loaded == "yes") {
            preloader_wrapper.remove();
        }
    }
    /**
     * endPreloader
     * *****************************************/

    /** *****************************************
     * Slimmenu
     * */
    jQuery(".slimmenu").slimmenu({
        resizeWidth: '991',
        collapserTitle: '',
        animSpeed: 'fast',
        indentChildren: true,
        childrenIndenter: '&raquo;'
    });

    /**
     * endSlimmenu
     * ******************************************/

    /** *****************************************
     * @library Call Functions
     * */

    jQuery('a.popup').fancybox();

    initSlider();
    /**
     * @endLibrary
     * ******************************************/

    /** *****************************************
     * @additional Lead Scripts
     * */
    jQuery('.social-media a').attr('target', '_blank');
    /**
     * @endAdditional
     * ******************************************/
});

function initSlider() {
    /** ***************************************
     * Slider
     * */
    jQuery(".content-slider").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        infinite: true,
        autoplay: true,
        fade: true
    });

    jQuery(".content-slider-secondary").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        infinite: true,
        autoplay: true
    });

    jQuery(".content-carousel").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        infinite: true,
        autoplay: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    jQuery(".content-carousel-secondary").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        infinite: true,
        autoplay: true
    });

    jQuery(".list-carousel").slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        infinite: true,
        autoplay: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 540,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    jQuery(".slider-main").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false
    });

    jQuery(".slider-thumbnail").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-main',
        dots: false,
        arrows: false,
        focusOnSelect: true
    });

    /** endSlider
     * *****************************************/

    /** ***************************************
     * Viewport
     * */
    var viewport = jQuery(".viewport-animate");
    viewport.each(function () {
        if (isOnScreen(this) === true) {
            jQuery(this).addClass('in-view').find(".animate").addClass('animate-active');
        }
    });
    window.addEventListener('scroll', function (e) {
        viewport.each(function () {
            if (isOnScreen(this) === true) {
                jQuery(this).addClass('in-view').find(".animate").addClass('animate-active');
            }
        });
    });
    /**
     * endViewport
     * ****************************************/
}

/**
 * @Visible_on_viewport
 * This function is to fire an event when targeted element comes to viewport up
 * */
function isOnScreen(elem) {
    // if the element doesn't exist, abort
    if (elem.length == 0) {
        return;
    }
    var $window = jQuery(window)
    var viewport_top = $window.scrollTop()
    var viewport_height = $window.height()
    var viewport_bottom = viewport_top + viewport_height
    var $elem = jQuery(elem)
    var top = $elem.offset().top
    var height = $elem.height()
    var bottom = top + height

    return (top >= viewport_top && top < viewport_bottom) ||
        (bottom > viewport_top && bottom <= viewport_bottom) ||
        (height > viewport_height && top <= viewport_top && bottom >= viewport_bottom)
}

/**
 * @endVisible_on_viewport
 * */