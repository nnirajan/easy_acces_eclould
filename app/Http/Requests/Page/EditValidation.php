<?php

namespace App\Http\Requests\Page;

use Illuminate\Foundation\Http\FormRequest;

class EditValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|unique:pages,name,'.$this->id,
            'page' => 'required',
            'url' => 'required|unique:pages,url,'.$this->id,
            'short_description' => 'required',
            'description' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Please add name.',
            'page.required' => 'Please add page.',
            'url.required' => 'Please add an url.',
            'short_description.required' => 'Please add short description.',
            'description.required' => 'Please add description.',
        ];
    }
}
