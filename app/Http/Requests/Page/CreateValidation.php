<?php

namespace App\Http\Requests\Page;

use Illuminate\Foundation\Http\FormRequest;

class CreateValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:pages',
            'page' => 'required',
            'url' => 'required|unique:pages',
            'short_description' => 'required',
            'description' => 'required',

        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Please add name.',
            'page.required' => 'Please add page.',
            'url.required' => 'Please add an url.',
            'short_description.required' => 'Please add short description.',
            'description.required' => 'Please add description.',
        ];
    }
}
