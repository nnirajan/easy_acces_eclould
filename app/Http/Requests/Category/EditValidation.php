<?php

namespace App\Http\Requests\Category;

use Illuminate\Foundation\Http\FormRequest;

class EditValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required',
            'status' => 'required',

        ];
    }
    public function messages()
    {
        return [
            'title.required' => 'Please add title.',
            'status.required' => 'Select status',

        ];
    }
}
