<?php

namespace App\Http\Requests\Contactus;

use Illuminate\Foundation\Http\FormRequest;

class CreateValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'message' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Please add name.',
            'email.required' => 'Please add email.',
            'phone.required' => 'Please add phone.',
            'message.required' => 'Please add message.',
        ];
    }
}
