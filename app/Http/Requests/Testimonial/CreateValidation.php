<?php

namespace App\Http\Requests\Testimonial;

use Illuminate\Foundation\Http\FormRequest;

class CreateValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'testimonial_image' => 'required',
            'office' => 'required',
            'position' => 'required',
            'display_rank' => 'required|integer',
            'testimonials' => 'required',
            'status' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Please add name.',
            'testimonial_image.required' => 'Please add image.',
            'office.required' => 'Please add office.',
            'position.required' => 'Please add position.',
            'display_rank.required' => 'Please add display rank.',
            'testimonials.required' => 'Please add testimonials.',
            'status.required' => 'Select status',
        ];
    }
}
