<?php

namespace App\Http\Requests\Slider;

use Illuminate\Foundation\Http\FormRequest;

class EditValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|unique:sliders,name,'.$this->id,
            'details' => 'required',
            'slider_image' => 'required',
            'link' => 'required|unique:sliders,link,'.$this->id,
            'rank' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Please add name.',
            'details.required' => 'Please add details.',
            'slider_iage.required' => 'Please add image.',
            'link.required' => 'Please add link.',
            'rank.required' => 'Please add rank.',
        ];
    }
}
