<?php

namespace App\Http\Requests\Slider;

use Illuminate\Foundation\Http\FormRequest;

class CreateValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:sliders',
            'details' => 'required',
            'slider_image' => 'required',
            'link' => 'required|unique:sliders',
            'rank' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Please add name.',
            'details.required' => 'Please add details.',
            'slider_iage.required' => 'Please add image.',
            'link.required' => 'Please add link.',
            'rank.required' => 'Please add rank.',
        ];
    }
}
