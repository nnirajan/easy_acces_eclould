<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class EditValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|unique:clients,name,'.$this->id,
            'client_image' => 'required',
            'rank' => 'required',
            'status' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Please add name.',
            'client_image.required' => 'Please add image.',
            'rank.required' => 'Please add rank.',
            'status.required' => 'Please select option.',
        ];
    }
}
