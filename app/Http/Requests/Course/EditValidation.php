<?php

namespace App\Http\Requests\Course;

use Illuminate\Foundation\Http\FormRequest;

class EditValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|unique:courses,name,'.$this->id,
            'short_description' => 'required',
            'description' => 'required',
            'rank' => 'required',
            'course_image' => 'required',
            'icon' => 'required',
            'status' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Please add name.',
            'short_description.required' => 'Please add short description.',
            'description.required' => 'Please add description.',
            'rank.required' => 'Please add rank.',
            'course_image.required' => 'Please add image.',
            'icon.required' => 'Please add icon.',
            'status.required' => 'Please select option.',
        ];
    }
}
