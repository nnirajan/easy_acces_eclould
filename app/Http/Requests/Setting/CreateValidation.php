<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class CreateValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
//            'key' => 'required',
//            'value' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Please add name.',
//            'key.required' => 'Please add an key.',
//            'value.required' => 'Type value',
        ];
    }
}
