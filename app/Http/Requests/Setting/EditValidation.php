<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

class EditValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'address' => 'required',
            'facebook' => 'required',
            'googleplus' => 'required',
            'youtube' => 'required',
            'twitter' => 'required',
            'site_logo' => 'required',
            'clients' => 'required',
            'branches' => 'required',
            'partners' => 'required',
            'awards_winning' => 'required',
            'why_choose_us_title' => 'required',
            'why_choose_us_description' => 'required',
//            'key' => 'required',
//            'value' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Please add name.',
            'phone.required' => 'Please add phone',
            'email.required' => 'Please add email',
            'address.required' => 'Please add address',
            'facebook.required' => 'Please add facebook url',
            'googleplus.required' => 'Please add google plus url',
            'youtube.required' => 'Please add youtube url',
            'twitter.required' => 'Please add twitter url',
            'site_logo.required' => 'Please add site logo',
            'clients.required' => 'Please add number of clients ',
            'branches.required' => 'Please add number of branches',
            'partners.required' => 'Please add number of partners',
            'awards_winning.required' => 'Please add number of awards winning',
            'why_choose_us_title.required' => 'Please add why choose us title',
            'why_choose_us_description.required' => 'Please add why choose us description',
//            'key.required' => 'Please add an key.',
//            'value.required' => 'Type value',
        ];
    }
}
