<?php

namespace App\Http\Requests\Service;

use Illuminate\Foundation\Http\FormRequest;

class CreateValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:services',
            'display_footer' => 'required',
            'feature_key' => 'required',
            'description' => 'required',
            'rank' => 'required',
            'service_image' => 'required',
            'icon' => 'required',
            'status' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Please add name.',
            'display_footer.required' => 'Please select option.',
            'feature_key.required' => 'Please select option.',
            'description.required' => 'Please add description.',
            'rank.required' => 'Please add rank.',
            'service_image.required' => 'Please add image.',
            'icon.required' => 'Please add icon.',
            'status.required' => 'Please select option.',
        ];
    }
}
