<?php

namespace App\Http\Requests\News;

use Illuminate\Foundation\Http\FormRequest;

class EditValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|unique:news,title,'.$this->id,
            'short_description' => 'required',
            'description' => 'required',
            'news_image' => 'required',
            'status' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'title.required' => 'Please add title.',
            'short_description.required' => 'Please add short description.',
            'description.required' => 'Please add description.',
            'news_image.required' => 'Please add image.',
            'status.required' => 'Please select option.',
        ];
    }
}
