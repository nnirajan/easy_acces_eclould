<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Models\Client;
use App\Models\Course;
use App\Models\News;
use App\Models\Page;
use App\Models\Post;
use App\Models\Service;
use App\Models\Setting;
use App\Models\Slider;
use App\Models\Testimonial;
use App\Models\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
//    public function index(Category $category){
//        $data = [];
//
//        $data['allnews'] = DB::table('categories')
//            ->join('posts', 'categories.id', '=', 'posts.category_id')
//            ->select('posts.*', 'categories.title AS c_title','categories.slug AS c_slug')
//            ->where('posts.status',1)
//            ->take(9)
//            ->orderBy('posts.created_at','desc')
//            ->get();
//
//        $data['top_story'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('top_news','1')
//            ->take(7)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        $data['main_story'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('main_news','1')
//            ->take(2)
//            ->orderBy('created_at','desc')
//            ->get();
//        $data['article'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('category_id',9)
//            ->take(3)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        $data['big_fashion'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('category_id',3)
//            ->take(1)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        $data['small_fashion'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('category_id',3)
//            ->take(2)
//            ->offset(1)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        $data['editorial'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('category_id',10)
//            ->take(1)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        $data['hot_story'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('latest_news','1')
//            ->take(2)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        $data['big_ent'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('category_id',2)
//            ->take(1)
//            ->orderBy('created_at','desc')
//            ->get();
//        $data['small_ent'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('category_id',2)
//            ->offset(1)
//            ->take(1)
//            ->orderBy('created_at','desc')
//            ->get();
//        $data['big_food'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('category_id',4)
//            ->take(1)
//            ->orderBy('created_at','desc')
//            ->get();
//        $data['small_food'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('category_id',4)
//            ->offset(1)
//            ->take(1)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        $data['interview'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('category_id',11)
//            ->take(1)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        $data['video'] = Video::select('id','title','url')
//            ->where('status',1)
//            ->take(1)
//            ->orderBy('created_at','desc')
//            ->get();
//        $data['video_small'] = Video::select('id','title','url')
//            ->where('status',1)
//            ->offset(1)
//            ->take(2)
//            ->orderBy('created_at','desc')
//            ->get();
//        $data['biography'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('category_id',12)
//            ->take(1)
//            ->orderBy('created_at','desc')
//            ->get();
//
//       //dd($data['video']);
//        return view('frontend.home.index',compact('data'));
//    }
//
//
//    public function video(Category $category){
//        $data = [];
//
//        $data['video'] = Video::select('id','title','url')
//            ->where('status',1)
//            ->take(1)
//            ->orderBy('created_at','desc')
//            ->get();
//        $data['video_small'] = Video::select('id','title','url')
//            ->where('status',1)
//            ->orderBy('created_at','desc')
//            ->paginate(10);
//        $data['sidebarNewsTop'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->take(3)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        $data['sidebarNewsBtn'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->offset(3)
//            ->take(5)
//            ->orderBy('created_at','desc')
//            ->get();
//        $data['allnews'] = DB::table('categories')
//            ->join('posts', 'categories.id', '=', 'posts.category_id')
//            ->select('posts.*', 'categories.title AS c_title','categories.slug AS c_slug')
//            ->where('posts.status',1)
//            ->take(9)
//            ->orderBy('posts.created_at','desc')
//            ->get();
//
//        return view('frontend.video.index',compact('data'));
//    }

    public function index(){
        $data=[];
        $data['sliders']=Slider::where('status','=','1')->get();
        $data['setting']=Setting::all();
        $data['aboutus']=Page::where('url','=','aboutus')->get();
//        dd($data['aboutus']);
        $data['services']=Service::where('status','=','1')->where('feature_key','=','1')->orderBy('rank','desc')->limit('4')->get();
        $data['courses']=Course::where('status','=','1')->orderBy('created_at','desc')->limit('6')->get();
        $data['clients']=Client::where('status','=','1')->orderBy('rank','desc')->get();
        $data['news']=News::where('status','=','1')->orderBy('created_at','desc')->limit('3')->get();
        $data['testimonials']=Testimonial::where('status','=','1')->orderBy('display_rank')->get();
        return view('frontend.home.index',compact('data'));
    }

}
