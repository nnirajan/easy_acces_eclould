<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Models\Post;
use App\Models\Service;
use App\Models\Course;
use App\Models\Client;
use App\Models\News;
use App\Models\Testimonial;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{
//    public function index($slug){
//        $data = [];
//
//        $data['allnews'] = DB::table('categories')
//            ->join('posts', 'categories.id', '=', 'posts.category_id')
//            ->select('posts.*', 'categories.title AS c_title','categories.slug AS c_slug')
//            ->where('posts.status',1)
//            ->take(9)
//            ->orderBy('posts.created_at','desc')
//            ->get();
//        $data['sidebarNewsTop'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->offset(1)
//            ->take(3)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        $data['sidebarNewsBtn'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->offset(4)
//            ->take(5)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        //$data['news'] = Post::find($id);
//        $data['news'] = DB::table('posts')
//            ->join('categories', 'categories.id', '=', 'posts.category_id')
//            ->select('posts.*', 'categories.title AS c_title','categories.slug AS c_slug')
//            ->where('posts.slug',$slug)
//            ->get();
//
//        //dd($data['news']);
//
//        $data['related_news'] = Post::where('category_id',$data['news'][0]->category_id)->get();
//
//
//        //dd($data['related_news']);
//        return view('frontend.news.index',compact('data'));
//    }

    public function index(){
        $data=[];
        $data['services']=Service::orderBy('created_at','desc')->limit('4')->get();
        $data['courses']=Course::orderBy('created_at','desc')->limit('6')->get();
        $data['clients']=Client::orderBy('rank','desc')->limit('5')->get();
        $data['news']=News::orderBy('created_at','desc')->limit('3')->get();
        $data['testimonials']=Testimonial::orderBy('display_rank')->limit('3')->get();
        $data['services_footer']=Service::where('display_footer','1')->get();
//        dd($data['services_footer']);

//        dd($data['testimonials']);
//        dd($data['news']);

//        dd($data['clients']);
//        $ddat['']
//        dd($data);
        return view('frontend.news.index',compact('data'));
    }

    public function show($id){
        $data['news']=News::find($id);
        return view('frontend.news.show',compact('data'));
    }
}
