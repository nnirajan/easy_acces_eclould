<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
//    public function index(Category $category){
//        $data = [];
//        $a = $category->slug;
//
//        $data['cat_post'] = DB::table('posts')
//            ->join('categories', 'categories.id', '=', 'posts.category_id')
//            ->select('posts.*', 'categories.title AS c_title')
//            ->where('categories.slug',$a)
//            ->where('categories.status',1)
//            ->paginate(10);
//
//
//        $data['allnews'] = DB::table('categories')
//            ->join('posts', 'categories.id', '=', 'posts.category_id')
//            ->select('posts.*', 'categories.title AS c_title','categories.slug AS c_slug')
//            ->where('posts.status',1)
//            ->take(9)
//            ->orderBy('posts.created_at','desc')
//            ->get();
//
//        $data['sidebarNewsTop'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->take(3)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        $data['sidebarNewsBtn'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->offset(3)
//            ->take(5)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        //dd($data['cat_post']);
//
//        return view('frontend.category.index',compact('data'));
//    }

    public function index(){
        $data=[];
        return view('frontend.home',compact('data'));
    }
}
