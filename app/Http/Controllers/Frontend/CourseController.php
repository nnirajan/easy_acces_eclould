<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Models\Client;
use App\Models\Course;
use App\Models\News;
use App\Models\Post;
use App\Models\Service;
use App\Models\Testimonial;
use App\Models\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CourseController extends Controller
{
//    public function index(Category $category){
//        $data = [];
//
//        $data['allnews'] = DB::table('categories')
//            ->join('posts', 'categories.id', '=', 'posts.category_id')
//            ->select('posts.*', 'categories.title AS c_title','categories.slug AS c_slug')
//            ->where('posts.status',1)
//            ->take(9)
//            ->orderBy('posts.created_at','desc')
//            ->get();
//
//        $data['top_story'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('top_news','1')
//            ->take(7)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        $data['main_story'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('main_news','1')
//            ->take(2)
//            ->orderBy('created_at','desc')
//            ->get();
//        $data['article'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('category_id',9)
//            ->take(3)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        $data['big_fashion'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('category_id',3)
//            ->take(1)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        $data['small_fashion'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('category_id',3)
//            ->take(2)
//            ->offset(1)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        $data['editorial'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('category_id',10)
//            ->take(1)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        $data['hot_story'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('latest_news','1')
//            ->take(2)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        $data['big_ent'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('category_id',2)
//            ->take(1)
//            ->orderBy('created_at','desc')
//            ->get();
//        $data['small_ent'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('category_id',2)
//            ->offset(1)
//            ->take(1)
//            ->orderBy('created_at','desc')
//            ->get();
//        $data['big_food'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('category_id',4)
//            ->take(1)
//            ->orderBy('created_at','desc')
//            ->get();
//        $data['small_food'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('category_id',4)
//            ->offset(1)
//            ->take(1)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        $data['interview'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('category_id',11)
//            ->take(1)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        $data['video'] = Video::select('id','title','url')
//            ->where('status',1)
//            ->take(1)
//            ->orderBy('created_at','desc')
//            ->get();
//        $data['video_small'] = Video::select('id','title','url')
//            ->where('status',1)
//            ->offset(1)
//            ->take(2)
//            ->orderBy('created_at','desc')
//            ->get();
//        $data['biography'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->where('category_id',12)
//            ->take(1)
//            ->orderBy('created_at','desc')
//            ->get();
//
//       //dd($data['video']);
//        return view('frontend.home.index',compact('data'));
//    }
//
//
//    public function video(Category $category){
//        $data = [];
//
//        $data['video'] = Video::select('id','title','url')
//            ->where('status',1)
//            ->take(1)
//            ->orderBy('created_at','desc')
//            ->get();
//        $data['video_small'] = Video::select('id','title','url')
//            ->where('status',1)
//            ->orderBy('created_at','desc')
//            ->paginate(10);
//        $data['sidebarNewsTop'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->take(3)
//            ->orderBy('created_at','desc')
//            ->get();
//
//        $data['sidebarNewsBtn'] = Post::select('id','title','slug','short_description','image','category_id','image_title')
//            ->where('status',1)
//            ->offset(3)
//            ->take(5)
//            ->orderBy('created_at','desc')
//            ->get();
//        $data['allnews'] = DB::table('categories')
//            ->join('posts', 'categories.id', '=', 'posts.category_id')
//            ->select('posts.*', 'categories.title AS c_title','categories.slug AS c_slug')
//            ->where('posts.status',1)
//            ->take(9)
//            ->orderBy('posts.created_at','desc')
//            ->get();
//
//        return view('frontend.video.index',compact('data'));
//    }

    public function index(){
        $data=[];
//        $data['services']=Service::orderBy('created_at','desc')->get();
        $data['courses']=Course::orderBy('created_at','desc')->get();
//        $data['clients']=Client::orderBy('rank','desc')->limit('5')->get();
//        $data['news']=News::orderBy('created_at','desc')->limit('3')->get();
//        $data['testimonials']=Testimonial::orderBy('display_rank')->limit('3')->get();
//        $data['services_footer']=Service::where('display_footer','1')->get();
//        dd($data['services_footer']);

//        dd($data['testimonials']);
//        dd($data['news']);

//        dd($data['clients']);
//        $ddat['']
//        dd($data);
        return view('frontend.course.index',compact('data'));
    }

    function  show($id){
//        $data['page_title'] = 'View Details  Category';
//        $data['category'] = Category::find($id);
        $data['course']=Course::find($id);
        return view('frontend.course.show',compact('data'));
    }

}
