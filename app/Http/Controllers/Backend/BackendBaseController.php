<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use AppHelper;
use View;
class BackendBaseController extends Controller
{
    protected $success_message = 'success_message';
    protected $error_message = 'error_message';

    protected function loadDataToView($view_path){
        View::composer($view_path, function ($view) {
            $view->with('base_route',$this->base_route);
            $view->with('view_path',$this->view_path);
            $view->with('panel',$this->panel);
            $view->with('folder_name', property_exists($this, 'folder_name' )?$this->folder_name:'');
            $view->with('trans_path',$this->trans_path);
        });

        return $view_path;

    }

    protected function invalidRequest($message = 'Invalid Request !'){
        request()->session()->flash('error_message',$message);
        return redirect()->route($this->base_route);
    }
}
