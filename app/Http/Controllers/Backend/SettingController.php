<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Setting\CreateValidation;
use App\Http\Requests\Setting\EditValidation;

use App\Models\Category;
use App\Models\Post;
use App\Models\Reporter;
use App\Models\Setting;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use AppHelper, view, Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SettingController extends BackendBaseController
{
    protected $base_route = 'backend.setting';
    protected $view_path = 'backend.setting';
    protected $panel = 'Setting';
    protected $folder_path;
    protected $folder_name = 'setting';
    protected $trans_path = 'backend/setting/general.';


    public function __construct()
    {
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
//        dd($this->folder_path);
    }

    public function index(){
        $data = [];

        $data['rows'] = Setting::all();

       /* $data['categories'] = Category::select('id','title')
            ->Active()
            ->orderBy('title')
            ->pluck('title','id');

        $data['rows'] = Post::select('id','title','category_id','status','image','created_at','reporter_id')
            ->get();*/
        //dd($data);

        return view($this->loadDataToView($this->view_path.'.index'),compact('data'));
    }

    public function create(){
        $data = [];
        return view($this->loadDataToView($this->view_path.'.create'),compact('data'));
    }
    public function store(CreateValidation $request){
        if($request->hasFile('sitelogo')){
            $file=$request->file('sitelogo');
//            dd($file);
            $filename=uniqid().'_'.$file->getClientOriginalName();
            $file->move('images/setting',$filename);
            $request->request->add(['site_logo'=>$filename]);
        }
        $request->request->add(['created_by' => auth()->user()->id]);
        Setting::create($request->all());
        $request->session()->flash($this->success_message, $this->panel.' Added Successfully');
        return redirect()->route($this->base_route);
    }

    public function show($id){
        $data = [];

        if (!$data['row'] =  Setting::find($id))
            return parent::invalidRequest();
        $data['base_route'] = $this->base_route;
//        dd($data);
        return view(parent::loadDataToView($this->view_path.'.show'),compact('data'));
    }

    public function edit(Request $request, $id){
        $data = [];
        if (!$data['row'] =  Setting::find($id))
            return parent::invalidRequest();
        $data['base_route'] = $this->base_route;
        return view(parent::loadDataToView($this->view_path.'.edit'),compact('data'));
    }
    public function update(CreateValidation $request, $id){
        //dd($request->all());
        if (!$row =  Setting::find($id)){
            $request->session()->flash('error_message', 'Invalid Request !');
            return redirect()->route($this->base_route);
        }
        $request->request->add(['updated_by' => Auth::user()->id]);
        $row->update($request->all());
        $request->session()->flash($this->success_message, $this->panel.' Update Successfully');
        return redirect()->route($this->base_route);
    }
    public function delete(Request $request, $id){
        if (!$row =  Setting::find($id)){
            $request->session()->flash('message', 'Invalid Request !');
            return redirect()->route($this->base_route);
        }
        $row->delete();
        $request->session()->flash($this->success_message, $this->panel.' Delete Successfully');
        return redirect()->route($this->base_route);
    }
}
