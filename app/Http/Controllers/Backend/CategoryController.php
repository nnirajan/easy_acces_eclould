<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Category\CreateValidation;
use App\Http\Requests\Category\EditValidation;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use AppHelper, view, Image;

class CategoryController extends BackendBaseController
{
    protected  $base_route = 'backend.category';
    protected $view_path = 'backend.category';
    protected $panel = 'Category';
    protected $folder_path;
    protected $folder_name = 'category';
    protected $trans_path = 'backend/category/general.';


    public function __construct()
    {
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
        // dd($this->folder_path);
    }

    public function index(){
        $data = [];
        $data['rows'] = Category::all();

        return view($this->loadDataToView($this->view_path.'.index'),compact('data'));
    }

    public function create(){
        $data = [];
        return view($this->loadDataToView($this->view_path.'.create'),compact('data'));
    }
    public function store(CreateValidation $request){
        if ($request->hasFile('category_image'))
            $image_name = $this->UploadImages($request);
        else
            return parent::invalidRequest();
        $request->request->add(['image' => $image_name]);
        $request->request->add(['created_by' => auth()->user()->id]);
        Category::create($request->all());
        $request->session()->flash($this->success_message, $this->panel.' Added Successfully');
        return redirect()->route($this->base_route);
    }

    public function edit(Request $request, $id){
        $data = [];
        if (!$data['row'] =  Category::find($id))
            return parent::invalidRequest();
        $data['base_route'] = $this->base_route;
        return view(parent::loadDataToView($this->view_path.'.edit'),compact('data'));
    }
    public function update(EditValidation $request, $id){
        //dd($request->all());
        $row = Category::find($id);

        if ($request->hasFile('category_image')){

            $image_name = $this->UploadImages($request);
            //Remove old image from folder
            if (file_exists($this->folder_path.$row->image))
                unlink($this->folder_path.$row->image);

            foreach (config('epalika.image_dimensions.category.image') as  $dimension) {
                if (file_exists($this->folder_path.$dimension['width'].'_'.$dimension['height'].'_'.$row->image))
                    unlink($this->folder_path.$dimension['width'].'_'.$dimension['height'].'_'.$row->image);
            }

        }
        $request->request->add(['updated_by' => auth()->user()->id]);
        $request->request->add(['image' => isset($image_name)?$image_name:$row->image]);

        $row->update($request->all());

        $request->session()->flash($this->success_message, $this->panel.' Update Successfully');
        return redirect()->route($this->base_route);
    }
    public function delete(Request $request, $id){
        //dd($id);
        $row =  Category::find($id);
        $row->delete();
        $request->session()->flash($this->success_message, $this->panel.' Delete Successfully');
        return redirect()->route($this->base_route);
    }

    protected function UploadImages(Request $request){
        $image = $request->file('category_image');
        $image_name = rand(6785,9814).'_'. $image->getClientOriginalName();
        //dd($image_name);
        $image->move($this->folder_path, $image_name);
        //code for image resize
        foreach (config('epalika.image_dimensions.category.image') as  $dimension) {
            // open and resize an image file
            $img = Image::make($this->folder_path.$image_name)->resize($dimension['width'], $dimension['height']);

            // save the same file as jpg with default quality
            $img->save($this->folder_path.$dimension['width'].'_'.$dimension['height'].'_'.$image_name);
        }
        return $image_name;
    }
}
