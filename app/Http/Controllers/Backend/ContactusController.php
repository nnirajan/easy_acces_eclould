<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Client\CreateValidation;
use App\Http\Requests\Client\EditValidation;

use App\Models\Category;
use App\Models\Contactus;
use App\Models\Post;
use App\Models\Reporter;
use App\Models\Client;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use AppHelper, view, Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ContactusController extends BackendBaseController
{
    protected  $base_route = 'backend.contactus';
    protected $view_path = 'backend.contactus';
    protected $panel = 'Contact Us';
    protected $folder_path;
    protected $folder_name = 'contactus';
    protected $trans_path = 'backend/contactus/general.';


    public function __construct()
    {
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
//        dd($this->folder_path);
    }

    public function index(){
        $data = [];

        $data['rows'] = Contactus::all();

       /* $data['categories'] = Category::select('id','title')
            ->Active()
            ->orderBy('title')
            ->pluck('title','id');

        $data['rows'] = Post::select('id','title','category_id','status','image','created_at','reporter_id')
            ->get();*/
        //dd($data);

        return view($this->loadDataToView($this->view_path.'.index'),compact('data'));
    }

//    public function create(){
//        $data = [];
//        return view($this->loadDataToView($this->view_path.'.create'),compact('data'));
//    }
//    public function store(CreateValidation $request){
////        dd($request);
//        //upload image
//        if($request->hasFile('client_image')){
//            $file=$request->file('client_image');
////            dd($file);
//            $filename=uniqid().'_'.$file->getClientOriginalName();
//            $file->move('images/client',$filename);
//            $request->request->add(['image'=>$filename]);
//        }
//        $request->request->add(['created_by' => auth()->user()->id]);
//        Client::create($request->all());
////        dd($request->all());
//        $request->session()->flash($this->success_message, $this->panel.' Added Successfully');
//        return redirect()->route($this->base_route);
//    }

    public function show($id){
        $data = [];
        if (!$data['row'] =  Contactus::find($id))
            return parent::invalidRequest();
        $data['base_route'] = $this->base_route;
        return view(parent::loadDataToView($this->view_path.'.show'),compact('data'));
    }

//    public function edit(Request $request, $id){
//        $data = [];
//        if (!$data['row'] =  Client::find($id))
//            return parent::invalidRequest();
//        $data['base_route'] = $this->base_route;
//        return view(parent::loadDataToView($this->view_path.'.edit'),compact('data'));
//    }
//    public function update(EditValidation $request, $id){
//        //dd($request->all());
//        if (!$row =  Client::find($id)){
//            $request->session()->flash('error_message', 'Invalid Request !');
//            return redirect()->route($this->base_route);
//        }
//
//        //upload image
//        if($request->hasFile('client_image')){
//            $file=$request->file('client_image');
//            $filename=uniqid().'_'.$file->getClientOriginalName();
//            $file->move('images/client',$filename);
//            $request->request->add(['image'=>$filename]);
//        }
//
//        $request->request->add(['updated_by' => Auth::user()->id]);
//        $row->update($request->all());
//        $request->session()->flash($this->success_message, $this->panel.' Update Successfully');
//        return redirect()->route($this->base_route);
//    }
    public function delete(Request $request, $id){
        if (!$row =  Client::find($id)){
            $request->session()->flash('message', 'Invalid Request !');
            return redirect()->route($this->base_route);
        }
        $row->delete();
        $request->session()->flash($this->success_message, $this->panel.' Delete Successfully');
        return redirect()->route($this->base_route);
    }

    public function reply(Request $request,$id){
        $data=[];
        return view(parent::loadDataToView($this->view_path.'.reply'),compact('data'));

    }
}
