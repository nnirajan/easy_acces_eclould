<?php

namespace App\Providers;

use App\Models\Page;
use App\Models\Service;
use App\Models\Setting;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use phpDocumentor\Reflection\DocBlock\Tags\See;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        view()->composer(['frontend.includes.footer'],function ($view){
           $view
               ->with('services',Service::where('status','=','1')
                   ->where('display_footer','=','1')
                   ->orderBy('rank','desc')->get()
               );
           $view
               ->with('settings',Setting::limit('1')->get()
               );
            $view
                ->with('aboutus',Page::where('url','=','aboutus')->get()
                );
        });

        view()->composer(['frontend.includes.header'],function($view){
           $view
               ->with('settings',Setting::limit('1')->get()
               );
            $view
                ->with('aboutus',Page::where('url','=','aboutus')->get()
                );
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
