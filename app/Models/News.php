<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = ['id','title','short_description','description','image','status','created_by','updated_by',
        'created_by','updated_by'];

    protected $nullable = ['updated_by'];

    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = $value == 'active'?1:0;
    }
    public function getStatusAttribute($value)
    {
        return $value == 1?'active':'in-active';
    }

}
