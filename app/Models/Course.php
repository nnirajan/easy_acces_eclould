<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['id','name','icon','image','short_description','description','rank','status','created_by','updated_by',
        'created_by','updated_by'];

    protected $nullable = ['updated_by'];

    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = $value == 'active'?1:0;
    }
    public function getStatusAttribute($value)
    {
        return $value == 1?'active':'in-active';
    }

    public function setDisplayFooterAttribute($value)
    {
        $this->attributes['display_footer'] = $value == 'active'?1:0;
    }
    public function getDisplayFooterAttribute($value)
    {
        return $value == 1?'active':'in-active';
    }

    public function setFeatureKeyAttribute($value)
    {
        $this->attributes['status'] = $value == 'active'?1:0;
    }
    public function getFeatureKeyAttribute($value)
    {
        return $value == 1?'active':'in-active';
    }

}
