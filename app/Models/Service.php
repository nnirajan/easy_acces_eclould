<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = ['id','name','display_footer','feature_key','description','rank','image','icon','status','created_by','updated_by',
        'created_by','updated_by'];

    protected $nullable = ['updated_by'];

    public function setStatusAttribute($value)
    {
//        dd($value);
        $this->attributes['status'] = $value == 'active'?1:0;
//    dd($this->attributes['status']);
    }
    public function getStatusAttribute($value)
    {
        return $value == 1?'active':'in-active';
    }

    public function setDisplayFooterAttribute($value)
    {
        $this->attributes['display_footer'] = $value == 'active'?1:0;
    }
    public function getDisplayFooterAttribute($value)
    {
        return $value == 1?'active':'in-active';
    }

    public function setFeatureKeyAttribute($value)
    {
        $this->attributes['feature_key'] = $value == 'active'?1:0;
    }
    public function getFeatureKeyAttribute($value)
    {
        return $value == 1?'active':'in-active';
    }

}
