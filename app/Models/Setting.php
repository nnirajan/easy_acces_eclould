<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
//    protected $fillable = ['id','name','key','value','editable','status','created_by','updated_by',
//        'created_by','updated_by'];

    protected $fillable = ['id','name','phone','email','address','facebook','twitter','googleplus','youtube','status','site_logo','clients','awards_winning','partners','branches','why_choose_us_title','why_choose_us_description','created_by','updated_by',
        'created_by','updated_by'];

    protected $nullable = ['updated_by'];

    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = $value == 'active'?1:0;
    }
    public function getStatusAttribute($value)
    {
        return $value == 1?'active':'in-active';
    }
//
//    public function setEditableAttribute($value)
//    {
//        $this->attributes['editable'] = $value == 'active'?1:0;
//    }
//    public function getEditableAttribute($value)
//    {
//        return $value == 1?'active':'in-active';
//    }

}
