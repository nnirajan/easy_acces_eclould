<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['id','name','rank','status','created_by','updated_by','news_key','problem_key','emergency_key','product_key','icon','image'];


    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = $value == 'active'?1:0;
    }
    public function getStatusAttribute($value)
    {
        return $value == 1?'active':'in-active';
    }

    public function setDisplayFooterAttribute($value)
    {
        $this->attributes['display_footer'] = $value == 'active'?1:0;
    }
    public function getDisplayFooterAttribute($value)
    {
        return $value == 1?'active':'in-active';
    }

    public function setFeatureKeyAttribute($value)
    {
        $this->attributes['feature_key'] = $value == 'active'?1:0;
    }
    public function getFeatureKeyAttribute($value)
    {
        return $value == 1?'active':'in-active';
    }
}
