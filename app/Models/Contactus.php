<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contactus extends Model
{
    protected $table='contact_us';
    protected $fillable = ['id','name','email','phone','message','status','reply'];
    protected $nullable = ['updated_by','reply'];
    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = $value == 'active'?1:0;
    }
    public function getStatusAttribute($value)
    {
        return $value == 1?'active':'in-active';
    }

}
