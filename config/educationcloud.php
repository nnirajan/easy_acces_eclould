<?php
return [
    'image_dimensions' => [
        'ward' => [
            'image' => [

                [
                    'width' => 936,
                    'height' => 525
                ],
                [
                    'width' => 95,
                    'height' => 95
                ],
                [
                    'width' => 300,
                    'height' => 180
                ],

            ],
        ],
        'category' => [
            'image' => [

                [
                    'width' => 936,
                    'height' => 525
                ],
                [
                    'width' => 95,
                    'height' => 95
                ],
                [
                    'width' => 300,
                    'height' => 180
                ],

            ],
        ],
    ],
];