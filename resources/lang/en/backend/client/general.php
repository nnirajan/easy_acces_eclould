<?php
return[
    'menu' => [
        'category'  =>  'Client',
        'create'  =>  'Create',
        'list'  =>  'List',
    ],
    'message' => [

    ],
    'buttons' => [
        'submit'    =>  'Submit',
        'reset'    =>  'Reset',
        'update' => 'Update'
    ],
    'content'   => [
        'common' => [
            'action' => 'Action',
            'manager' => 'Client Manager',
            'created_at' => 'Created Date',
            'image' => 'Image',
            'list_category' => 'List of Category',
            'name' => 'Name',
            'sn' => 'S.N',
            'status' => 'Status'
        ],
        'list' => [
            'list' => 'List'
        ],
        'create' => [
            'create'    =>  'Create',
            'category_icon'    =>  'Category Icon',
            'date'    =>  'Published',
            'emergency'    =>  'Show the category in Emergency section',
            'image'    =>  'Image',
            'name'    =>  'Name',
            'news'    =>  'Show the category in Client section',
            'problem'    =>  'Show the category in Problem section',
            'rank'    =>  'Rank',
            'status'    =>  'Status',
        ],
        'view' => [
            'description' => 'Description',
            'value' => 'Value',
            'view_value' => 'View Value',

        ],
        'edit' => [
            'edit' => 'Edit',
        ],
        'delete' => [],
        'placeholder' => [
            'active'  =>  'Active',
            'icon'  =>  'Upload category icon',
            'image'  =>  'Upload an Image',
            'inactive'  =>  'Inactive',
            'name'  =>  'Type category name',
            'rank'  =>  'Type rank',
        ],

    ],
];
