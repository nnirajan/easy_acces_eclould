<div class="box-body">
    <div class="form-group">
        {!! Form::label('title', 'Title') !!}
        {!! Form::text('title',null, ["class" => "form-control"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'title'])
    </div>
    <div class="form-group">
        {!! Form::label('short_descriptioin', 'Short Description') !!}
        {!! Form::textarea('short_description',null, ["class" => "form-control"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'short_description'])
    </div>
    <div class="form-group">
        {!! Form::label('description', 'Description') !!}
        {!! Form::textarea('description',null, ["class" => "form-control"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'description'])
    </div>
    <div class="form-group">
        {!! Form::label('image', 'Image') !!}
        {!! Form::file('news_image',null, ["class" => "form-control"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'news_image'])
    </div>
</div>