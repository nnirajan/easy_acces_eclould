<div class="form-group"> {!! Form::label('status', 'Status',["class" => "radiostatus"]) !!}<br>
    <label class="radio-inline"> {!! Form::radio('status', 'active', true) !!}Active </label>
    <label class="radio-inline"> {!! Form::radio('status', 'in-active', false) !!}Inactive </label>
    @include('backend.includes.form_fields_validation',['fieldname' => 'status'])
</div>
