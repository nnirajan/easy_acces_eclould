<div class="box-body">
    <div class="form-group">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name',null, ["placeholder" => "Type Name", "class" => "form-control", "placeholder" => "Write Name"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'name'])
    </div>
    <div class="form-group">
        {!! Form::label('page', 'Page') !!}
        {!! Form::text('page',null, ["placeholder" => "Type Page", "class" => "form-control", "placeholder" => "Write Page"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'page'])
    </div>
    <div class="form-group">
        {!! Form::label('url', 'URL') !!}
        {!! Form::text('url',null, ["placeholder" => "Type URL", "class" => "form-control", "placeholder" => "Write URL"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'url'])
    </div>
    <div class="form-group">
        {!! Form::label('short_description', 'Short Description') !!}
        {!! Form::textarea('short_description',null, ["class" => "form-control"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'short_description'])
    </div>
    <div class="form-group">
        {!! Form::label('description', 'Description') !!}
        {!! Form::textarea('description',null, ["class" => "form-control"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'description'])
    </div>
    <div class="form-group">
        {!! Form::label('image', 'Image') !!}
        {!! Form::file('page_image',null, ["class" => "form-control"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'description'])
    </div>
</div>