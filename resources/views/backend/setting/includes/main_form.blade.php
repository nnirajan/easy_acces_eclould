<div class="box-body">
    <div class="form-group">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name',null, ["class" => "form-control", "placeholder" => "Write Name"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'name'])
    </div>
    <div class="form-group">
        {!! Form::label('phone', 'Phone') !!}
        {!! Form::text('phone',null, ["class" => "form-control", "placeholder" => "Write Phone"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'phone'])
    </div>
    <div class="form-group">
        {!! Form::label('email', 'Email') !!}
        {!! Form::email('email',null, ["class" => "form-control", "placeholder" => "Write Email"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'email'])
    </div>
    <div class="form-group">
        {!! Form::label('address', 'Address') !!}
        {!! Form::text('address',null, ["class" => "form-control", "placeholder" => "Write Address"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'address'])
    </div>

    <div class="form-group">
        {!! Form::label('facebook', 'Facebook url') !!}
        {!! Form::url('facebook',null, ["class" => "form-control", "placeholder" => "Write Facebook URL"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'facebook'])
    </div>
    <div class="form-group">
        {!! Form::label('twitter', 'Twitter url') !!}
        {!! Form::url('twitter',null, ["class" => "form-control", "placeholder" => "Write Twitter URL"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'twitter'])
    </div>
    <div class="form-group">
        {!! Form::label('youtube', 'Youtube Url') !!}
        {!! Form::url('youtube',null, ["class" => "form-control", "placeholder" => "Write Youtube URL"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'youtube'])
    </div>
    <div class="form-group">
        {!! Form::label('googleplus', 'Google+ Url') !!}
        {!! Form::url('googleplus',null, ["class" => "form-control", "placeholder" => "Write Google+ URL"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'googleplus'])
    </div>
    <div class="form-group">
        {!! Form::label('site_logo', 'Site Logo') !!}
        {!! Form::file('sitelogo',null, ["class" => "form-control", "placeholder" => "Write Google+ URL"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'site_logo'])
    </div>
    <div class="form-group">
        {!! Form::label('clients', 'Number of Clients') !!}
        {!! Form::number('clients',null, ["class" => "form-control", "placeholder" => "Write Number of Clients","min"=>"1"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'clients'])
    </div>
    <div class="form-group">
        {!! Form::label('partners', 'Number of Partners') !!}
        {!! Form::number('partners',null, ["class" => "form-control", "placeholder" => "Write Number of Partners","min"=>"1"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'partners'])
    </div>
    <div class="form-group">
        {!! Form::label('braches', 'Number of Branches') !!}
        {!! Form::number('branches',null, ["class" => "form-control", "placeholder" => "Write Number of Branches","min"=>"1"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'branches'])
    </div>
    <div class="form-group">
        {!! Form::label('awards_winning', 'Number of Awards Winning') !!}
        {!! Form::number('awards_winning',null, ["class" => "form-control", "placeholder" => "Write Number of Awards Winning"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'awards_winning'])
    </div>
    <div class="form-group">
        {!! Form::label('why_choose_us_title', 'Why Choose Us Title') !!}
        {!! Form::textarea('why_choose_us_title',null, ["class" => "form-control"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'why_choose_us_title'])
    </div>
    <div class="form-group">
        {!! Form::label('why_choose_us_description', 'Why Choose Us Description') !!}
        {!! Form::textarea('why_choose_us_description',null, ["class" => "form-control"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'why_choose_us_description'])
    </div>

</div>