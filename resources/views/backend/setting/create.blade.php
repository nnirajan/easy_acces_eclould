@extends('backend.layout.master')
@section('title','Create Setting')

@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>{{ trans(  $trans_path . 'content.common.manager') }}  <small class="breddes"><a href="{{ route($view_path) }}" class="btn btn-primary btn-block margin-bottom"><i class="fa fa-th-list" aria-hidden="true"></i>{{ trans(  $trans_path . 'menu.list') }}</a></small> </h1>
            <ol class="breadcrumb">
                @include($view_path.'.includes.breadcrumb-primary')
                <li class="active">Create</li>
            </ol>
        </section>
        <section class="content">
            <div class="row"> {!! Form::open(['route' => $base_route.'.store', 'method' => 'POST','files' => true]) !!}
                <div class="col-md-9">
                    <!-- general form elements -->
                    <div class="box box-primary"> @include('backend.includes.validation_message_error')
                        <div class="box-header with-border">
                            <h3 class="box-title">Create a {{ $panel }}</h3>
                        </div>
                    @include($view_path.'.includes.main_form')
                    <!-- /.box-body -->

                        <div class="box-footer fboxm">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check icheck"></i>Submit</button>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-undo icheck"></i>Reset</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        @include($view_path.'.includes.sidebar_main_form')
                    </div>
                </div>
                <!-- right column -->
            {!! Form::close() !!}
            <!--/.col (right) -->
            </div>

            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@section('js')
    @include('backend.includes.ckeditor')
@endsection