@extends('backend.layout.master')
@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1> {{ $panel }} Management <small class="breddes"><a href="{{ route($view_path) }}" class="btn btn-primary btn-block margin-bottom"><i class="fa fa-th-list" aria-hidden="true"></i>List</a></small> </h1>
            <ol class="breadcrumb">
                @include($view_path.'.includes.breadcrumb-primary')
                <li class="active">Edit</li>
            </ol>
        </section>
        <section class="content">
            <div class="row"> {!! Form::model($data['row'],['route' => [$base_route.'.update', $data['row']->id], 'method' => 'POST','files' => true]) !!}
                <div class="col-md-9">
                    <!-- general form elements -->
                    <div class="box box-primary"> @include('backend.includes.validation_message_error')
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit {{ $panel }}</h3>
                        </div>
                    @include($view_path.'.includes.main_form')
                    <!-- /.box-body -->

                        <div class="box-footer fboxm">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check icheck"></i>Submit</button>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-undo icheck"></i>Reset</button>
                        </div>
                    </div>
                </div>
                {{--<div class="col-md-3">--}}
                    {{--<!-- general form elements -->--}}
                    {{--<div class="box box-primary">--}}
                        {{--<div class="box-header with-border">--}}
                            {{--<h3 class="box-title">Status</h3>--}}
                        {{--</div>--}}
                        {{--<div class="box-body">--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="pustatus">--}}
                                    {{--<ul>--}}
                                        {{--<li><i class="fa fa-key"></i>Status: <span>Publish</span> | <a href="">Edit</a></li>--}}
                                        {{--<li><i class="fa fa-calendar"></i>Published: @php echo (date('d M Y')) @endphp</li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--@include($view_path.'.includes.sidebar_main_form') </div>--}}
                    {{--</div>--}}
                    {{--<!-- /.box -->--}}

                {{--</div>--}}
                <!-- right column -->
            {!! Form::close() !!}
            <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@section('js')
    @include('backend.includes.ckeditor')
@endsection