@extends('backend.layout.master')
@section('title','View Setting')
@section('css')
    <link rel="stylesheet" href="{{ asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/css/alert.css')}}">
@endsection
@section('content')
    <div class="content-wrapper" style="min-height: 916px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $panel }} Management
                <small class="breddes"><a href="{{ route($view_path) }}" class="btn btn-primary btn-block margin-bottom"><i class="fa fa-lists" aria-hidden="true"></i>List</a></small>
            </h1>
            <ol class="breadcrumb">
                @include($view_path.'.includes.breadcrumb-primary')
                <li class="active">List</li>
            </ol>
        </section>

        @include('backend.includes.flash_message')
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Details of {{ $panel }}</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                            @if($data['row']->count() == 0 )
                                                <tr>
                                                    <td class="bg bg-danger" colspan="2">Invalid Data</td>

                                                </tr>
                                            @else
                                                <tr>
                                                    <th width="25%">Nepali Name</th>
                                                    <td>{{$data['row']->name_np}}</td>
                                                </tr>
                                                <tr>
                                                    <th>English Name</th>
                                                    <td>{{$data['row']->name_en}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Ward No</th>
                                                    <td>{{$data['row']->ward_no}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Description</th>
                                                    <td>{!! $data['row']->description  !!}</td>
                                                </tr>
                                                <tr>
                                                    <th>Total Population</th>
                                                    <td>{{$data['row']->total_population}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Total Male</th>
                                                    <td>{{$data['row']->total_male}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Total Female</th>
                                                    <td>{{$data['row']->total_female}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Total Other</th>
                                                    <td>{{$data['row']->total_other}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Email</th>
                                                    <td>{{$data['row']->email}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Contact</th>
                                                    <td>{{$data['row']->contact}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Tole/Chowk</th>
                                                    <td>{{$data['row']->tole}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Latitude</th>
                                                    <td>{{$data['row']->latitude}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Longitude</th>
                                                    <td>{{$data['row']->longitude}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Address</th>
                                                    <td>{{$data['row']->address}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Area</th>
                                                    <td>{{$data['row']->area}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Image</th>
                                                    <td><img src="{{asset('images/ward/' .$data['row']->image )}}" height="300" width="300"></td>
                                                </tr>
                                                <tr>
                                                    <th>File</th>
                                                    <td><a href="{{asset('files/ward/' .$data['row']->file )}}"><i class="fa fa-pdf"></i>Download </a></td>
                                                </tr>
                                                <tr>
                                                    <th>Status</th>
                                                    <td>{{$data['row']->getStatusAttribute($data['row']->status)}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Ward Key</th>
                                                    <td>{{$data['row']->getStatusAttribute($data['row']->ward_key)}}</td>
                                                </tr>

                                                <tr>
                                                    <th>Published Date</th>
                                                    <td>{{ date('D, j M Y', strtotime($data['row']->created_at)) }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Updated Date</th>
                                                    <td>{{ date('D, j M Y', strtotime($data['row']->updated_at)) }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Published By</th>
                                                    <td>{{Auth::user($data['row']->created_by)->name}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Updated By</th>
                                                    <td>{{Auth::user($data['row']->updated_by)->name}}</td>
                                                </tr>
                                                <tr>
                                                    <td><a href="{{ route($base_route.'.edit', ['id' => $data['row']->id]) }}"><i class="glyphicon glyphicon-edit"></i></a></td>
                                                    <td><a class="sa-warning" href="{{ route($base_route.'.delete', ['id' => $data['row']->id]) }}"><i class="glyphicon glyphicon-trash"></i></a></td>
                                                </tr>

                                            @endif

                                        </table>
                                    </div>
                                </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="{{ asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('backend/js/alert.min.js') }}"></script>
    <script src="{{ asset('backend/js/alert.custom.js') }}"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@endsection