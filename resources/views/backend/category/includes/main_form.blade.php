<div class="box-body">
    <div class="form-group">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name',null, ["placeholder" => "Type Name ", "class" => "form-control"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'name'])
    </div>
    <div class="form-group">
        {!! Form::label('rank', 'Rank') !!}
        {!! Form::text('rank',null, ["placeholder" => "Type Rank", "class" => "form-control"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'rank'])

    </div>
    <div class="form-group">
        {!! Form::label('icon', 'Icon') !!}
        {!! Form::text('icon',null, ["placeholder" => "Type icon", "class" => "form-control"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'icon'])

    </div>

    @if(isset($data['row']))
        <div class="form-group">
            {!! Form::label('image', 'Existing image') !!}<br>
            @if($data['row']->image)
                <img src="{{ asset('images/'.$folder_name.'/'.$data['row']->image) }}" alt="" width="200" class="img-responsivess">
            @else
                <p>No Image</p>
            @endif

        </div>
    @endif

    <div class="form-group"> {!! Form::label('category_image', 'Image : select an image') !!}
        {!! Form::file('category_image') !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'category_image'])
    </div>


</div>