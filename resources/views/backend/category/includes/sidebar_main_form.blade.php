<div class="form-group"> {!! Form::label('status', 'Status',["class" => "radiostatus"]) !!}<br>
    <label class="radio-inline"> {!! Form::radio('status', 'active', true) !!}Active </label>
    <label class="radio-inline"> {!! Form::radio('status', 'in-active', false) !!}Inactive </label>
    @include('backend.includes.form_fields_validation',['fieldname' => 'status'])
</div>
@if(isset($data['row']))
    <div class="form-group"> {!! Form::label('category_type', 'Tick Category Type') !!}
        <div class="checkbox cboxcus">
            <label> {!! Form::checkbox('news_key','1', $data['row']->news_key == 1? true:false) !!} News Category</label>
            <label> {!! Form::checkbox('problem_key','1', $data['row']->problem_key == 1?true:false) !!}Problem Category</label>
            <label> {!! Form::checkbox('emergency_key','1', $data['row']->emergency_key == 1?true:false) !!}Emergency Category</label>
            <label> {!! Form::checkbox('product_key','1', $data['row']->product_key == 1?true:false) !!}Product Category</label>


        </div>
    </div>

@else

    <div class="form-group"> {!! Form::label('category_type', 'Tick Category Type') !!}
        <div class="checkbox cboxcus">
            <label> {!! Form::checkbox('news_key',1, false) !!} News Category</label>
            <label> {!! Form::checkbox('problem_key',1, false) !!}Problem Category</label>
            <label> {!! Form::checkbox('emergency_key',1, false) !!}Emergency Category</label>
            <label> {!! Form::checkbox('product_key',1, false) !!}Product Category</label>

        </div>
    </div>
@endif