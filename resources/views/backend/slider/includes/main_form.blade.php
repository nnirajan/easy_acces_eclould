<div class="box-body">
    <div class="form-group">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name',null, ["placeholder" => "Type Name", "class" => "form-control", "placeholder" => "Write Name"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'name'])
    </div>
    <div class="form-group">
        {!! Form::label('details', 'Details') !!}
        {!! Form::textarea('details',null, ["placeholder" => "Type Page", "class" => "form-control", "placeholder" => "Write Details"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'details'])
    </div>
    <div class="form-group">
        {!! Form::label('link', 'Link') !!}
        {!! Form::url('link',null, ["placeholder" => "Type URL", "class" => "form-control", "placeholder" => "Write link"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'link'])
    </div>
    <div class="form-group">
        {!! Form::label('rank', 'Rank') !!}
        {!! Form::number('rank',null, ["placeholder" => "Type URL", "class" => "form-control", "placeholder" => "Write Rank","min"=>1]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'rank'])
    </div>
    <div class="form-group">
        {!! Form::label('image', 'Image') !!}
        {!! Form::file('slider_image',null, ["placeholder" => "Type URL", "class" => "form-control", "placeholder" => "Write link"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'slider_image'])
    </div>
</div>