@extends('backend.layout.master')
@section('title','List Page')
@section('css')
    <link rel="stylesheet" href="{{ asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/css/alert.css')}}">
@endsection
@section('content')
    <div class="content-wrapper" style="min-height: 916px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $panel }} Management
                <small class="breddes"><a href="{{ route($view_path.'.create') }}" class="btn btn-primary btn-block margin-bottom"><i class="fa fa-plus" aria-hidden="true"></i>Create</a></small>
            </h1>
            <ol class="breadcrumb">
                @include($view_path.'.includes.breadcrumb-primary')
                <li class="active">List</li>
            </ol>
        </section>

        @include('backend.includes.flash_message')
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">List of {{ $panel }}</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row"><div class="col-sm-12"><table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                            <thead>
                                            <tr role="row">
                                                <th>S.N</th>
                                                <th>Name</th>
                                                <th>Image</th>
                                                <th>Link</th>
                                                <th>Rank</th>
                                                <th>Status</th>
                                                <th style="width: 80px;">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php $i = 1; @endphp
                                            @if($data['rows'] == $data['rows']->count() > 0 )

                                                @foreach($data['rows'] as $row)
                                                <tr role="row" class="odd">
                                                    <td>{{ $i++ }}</td>
                                                <td class="sorting_1">{{ $row->name }}</td>
                                                <td><img src="{{asset('images/slider/'.$row->image)}}" alt="{{$row->name}}" height="100" width="100" ></td>
                                                <td>{{ $row->link }}</td>
                                                <td>{{ $row->rank }}</td>
                                                @if($row->status == 'active')
                                                    <td><span class="label label-success">Active</span></td>
                                                @else
                                                    <td><span class="label label-warning">Inactive</span></td>
                                                @endif
                                                <td>
                                                    <div class="bedit">
                                                        <a href="{{ route($base_route.'.show', ['id' => $row->id]) }}"><i class="glyphicon glyphicon-eye-open"></i></a>
                                                        <a href="{{ route($base_route.'.edit', ['id' => $row->id]) }}"><i class="glyphicon glyphicon-edit"></i></a>
                                                        <a class="sa-warning" href="{{ route($base_route.'.delete', ['id' => $row->id]) }}"><i class="glyphicon glyphicon-trash"></i></a>
                                                    </div>
                                                </td>
                                                </tr>
                                                @endforeach
                                                @else

                                                <tr><td colspan="6">Data Not Found</td></tr>

                                            @endif


                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>S.N</th>
                                                <th>Name</th>
                                                <th>Page</th>
                                                <th>URL</th>
                                                <th>Short Description</th>
                                                <th>Status</th>
                                                <th style="width: 80px;">Action</th>
                                            </tr>
                                            </tfoot>
                                        </table></div></div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="{{ asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('backend/js/alert.min.js') }}"></script>
    <script src="{{ asset('backend/js/alert.custom.js') }}"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@endsection