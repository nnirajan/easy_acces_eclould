<div class="box-header with-border">
    <h3 class="box-title">Status</h3>
</div>

<div class="form-group">
    <label class="radio-inline"> {!! Form::radio('status', 'active', true) !!}Active </label>
    <label class="radio-inline"> {!! Form::radio('status', 'in-active', false) !!}Inactive </label>
    @include('backend.includes.form_fields_validation',['fieldname' => 'status'])
</div>



