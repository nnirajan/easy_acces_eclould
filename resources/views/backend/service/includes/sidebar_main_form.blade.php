<div class="box-header with-border">
    <h3 class="box-title">Status</h3>
</div>

<div class="form-group">
    <label class="radio-inline"> {!! Form::radio('status', 'active', true) !!}Active </label>
    <label class="radio-inline"> {!! Form::radio('status', 'in-active', false) !!}Inactive </label>
    @include('backend.includes.form_fields_validation',['fieldname' => 'status'])
</div>

<div class="box-header with-border">
    <h3 class="box-title">Display in the foooter</h3>
</div>
<div class="form-group">
    <label class="radio-inline"> {!! Form::radio('display_footer', 'active', true) !!}Active </label>
    <label class="radio-inline"> {!! Form::radio('display_footer', 'in-active', false) !!}Inactive </label>
    @include('backend.includes.form_fields_validation',['fieldname' => 'display_footer'])
</div>

<div class="box-header with-border">
    <h3 class="box-title">Feature key</h3>
</div>
<div class="form-group">
    <label class="radio-inline"> {!! Form::radio('feature_key', 'active', true) !!}Active </label>
    <label class="radio-inline"> {!! Form::radio('feature_key', 'in-active', false) !!}Inactive </label>
        @include('backend.includes.form_fields_validation',['fieldname' => 'feature_key'])
</div>



