<div class="box-body">
    <div class="form-group">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name',null, ["placeholder" => "Type Name", "class" => "form-control", "placeholder" => "Write Name"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'name'])
    </div>
    <div class="form-group">
        {!! Form::label('description', 'Description') !!}
        {!! Form::textarea('description',null, ["placeholder" => "Type Page", "class" => "form-control", "placeholder" => "Write Details"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'description'])
    </div>
    <div class="form-group">
        {!! Form::label('rank', 'Rank') !!}
        {!! Form::number('rank',null, ["placeholder" => "Type URL", "class" => "form-control", "placeholder" => "Write Rank","min"=>1]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'rank'])
    </div>
    <div class="form-group">
        {!! Form::label('image', 'Image') !!}
        {!! Form::file('service_image',null, ["class" => "form-control", "placeholder" => "Write Name"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'service_image'])
    </div>
    <div class="form-group">
        {!! Form::label('icon', 'Icon') !!}
        {!! Form::text('icon',null, ["class" => "form-control"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'icon'])
    </div>
</div>