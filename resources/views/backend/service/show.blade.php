@extends('backend.layout.master')
@section('title','View Service')
@section('css')
    <link rel="stylesheet" href="{{ asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/css/alert.css')}}">
@endsection
@section('content')
    <div class="content-wrapper" style="min-height: 916px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $panel }} Management
                <small class="breddes"><a href="{{ route($view_path) }}" class="btn btn-primary btn-block margin-bottom"><i class="fa fa-lists" aria-hidden="true"></i>List</a></small>
            </h1>
            <ol class="breadcrumb">
                @include($view_path.'.includes.breadcrumb-primary')
                <li class="active">List</li>
            </ol>
        </section>

        @include('backend.includes.flash_message')
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Details of {{ $panel }}</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                            @if($data['row']->count() == 0 )
                                                <tr>
                                                    <td class="bg bg-danger" colspan="2">Invalid Data</td>

                                                </tr>
                                            @else
                                                <tr>
                                                    <th width="25%">Name</th>
                                                    <td>{{$data['row']->name}}</td>
                                                </tr>
                                                <tr>
                                                    <th width="25%">Display Footer</th>
                                                    @if($data['row']->display_footer == 'active')
                                                        <td><span class="label label-success">Active</span></td>
                                                    @else
                                                        <td><span class="label label-warning">Inactive</span></td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <th width="25%">Feature Key</th>
                                                    @if($data['row']->feature_key == 'active')
                                                        <td><span class="label label-success">Active</span></td>
                                                    @else
                                                        <td><span class="label label-warning">Inactive</span></td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <th width="25%">Description</th>
                                                    <td>{{$data['row']->description}}</td>
                                                </tr>
                                                <tr>
                                                    <th width="25%">Image</th>
                                                    <td><img src="{{asset('images/service/image/'.$data['row']->image)}}" height="100" width="100" alt="{{$data['row']->name}}" ></td>
                                                </tr>
                                                <tr>
                                                    <th width="25%">Link</th>
                                                    <td>{{$data['row']->link}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Rank</th>
                                                    <td>{{$data['row']->rank}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Status</th>
                                                    @if($data['row']->status == 'active')
                                                        <td><span class="label label-success">Active</span></td>
                                                    @else
                                                        <td><span class="label label-warning">Inactive</span></td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <th>Published Date</th>
                                                    <td>{{ date('D, j M Y', strtotime($data['row']->created_at)) }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Updated Date</th>
                                                    <td>{{ date('D, j M Y', strtotime($data['row']->updated_at)) }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Published By</th>
                                                    <td>{{Auth::user($data['row']->created_by)->name}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Updated By</th>
                                                    <td>{{Auth::user($data['row']->updated_by)->name}}</td>
                                                </tr>
                                                <tr>
                                                    <td><a href="{{ route($base_route.'.edit', ['id' => $data['row']->id]) }}"><i class="glyphicon glyphicon-edit"></i></a></td>
                                                    <td><a class="sa-warning" href="{{ route($base_route.'.delete', ['id' => $data['row']->id]) }}"><i class="glyphicon glyphicon-trash"></i></a></td>
                                                </tr>

                                            @endif

                                        </table>
                                    </div>
                                </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="{{ asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('backend/js/alert.min.js') }}"></script>
    <script src="{{ asset('backend/js/alert.custom.js') }}"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@endsection