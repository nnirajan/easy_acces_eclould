 <div class="box-body">
    <div class="form-group">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name',null, ["class" => "form-control", "placeholder" => "Write Name"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'title'])
    </div>
    <div class="form-group">
        {!! Form::label('image', 'Image') !!}
        {!! Form::file('testimonial_image',null, ["class" => "form-control"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'testimonial_image'])
    </div>
     <div class="form-group">
         {!! Form::label('office', 'Office') !!}
         {!! Form::text('office',null, ["class" => "form-control", "placeholder" => "Write Office"]) !!}
         @include('backend.includes.form_fields_validation',['fieldname' => 'office'])
     </div>
     <div class="form-group">
         {!! Form::label('position', 'Position') !!}
         {!! Form::text('position',null, ["class" => "form-control", "placeholder" => "Write Position"]) !!}
         @include('backend.includes.form_fields_validation',['fieldname' => 'position'])
     </div>
     <div class="form-group">
         {!! Form::label('display_rank', 'Display Rank') !!}
         {!! Form::number('display_rank',null, ["placeholder" => "Type URL", "class" => "form-control", "placeholder" => "Write Display Rank","min"=>1]) !!}
         @include('backend.includes.form_fields_validation',['fieldname' => 'display_rank'])
     </div>
     <div class="form-group">
         {!! Form::label('testimonials', 'Testimonials') !!}
         {!! Form::textarea('testimonials',null, ["placeholder" => "Type URL", "class" => "form-control", "placeholder" => "Write Testimonials","min"=>1]) !!}
         @include('backend.includes.form_fields_validation',['fieldname' => 'testimonials'])
     </div>
</div>