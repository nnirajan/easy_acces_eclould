<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                {{--<img src="{{asset('backend/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">--}}
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>

            <li>
                <a href="{{ route('backend.dashboard') }}"><i class="fa fa-bars"></i>Dashboard</a>
            </li>



            {{--Pages--}}
            <li class="treeview {!! request()->is('backend/page*')?'active menu-open':"" !!}">
                <a href="#">
                    <i class="fa fa-edit"></i>
                    <span>Pages</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! request()->is('backend/page/create')?'active':"" !!}"><a href="{{ route('backend.page.create') }}"><i class="fa fa-circle-o"></i>Create</a></li>
                    <li class="{!! request()->is('backend/page')?'active':"" !!}"><a href="{{ route('backend.page') }}"><i class="fa fa-circle-o"></i>List</a></li>
                </ul>
            </li>

            {{--Slider--}}
            <li class="treeview {!! request()->is('backend/slider*')?'active menu-open':"" !!}">
                <a href="#">
                    <i class="fa fa-edit"></i>
                    <span>Slider</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! request()->is('backend/slider/create')?'active':"" !!}"><a href="{{ route('backend.slider.create') }}"><i class="fa fa-circle-o"></i>Create</a></li>
                    <li class="{!! request()->is('backend/slider')?'active':"" !!}"><a href="{{ route('backend.slider') }}"><i class="fa fa-circle-o"></i>List</a></li>
                </ul>
            </li>

            {{--Service--}}
            <li class="treeview {!! request()->is('backend/service*')?'active menu-open':"" !!}">
                <a href="#">
                    <i class="fa fa-edit"></i>
                    <span>Service</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! request()->is('backend/service/create')?'active':"" !!}"><a href="{{ route('backend.service.create') }}"><i class="fa fa-circle-o"></i>Create</a></li>
                    <li class="{!! request()->is('backend/service')?'active':"" !!}"><a href="{{ route('backend.service') }}"><i class="fa fa-circle-o"></i>List</a></li>
                </ul>
            </li>

            {{--Course--}}
            <li class="treeview {!! request()->is('backend/course*')?'active menu-open':"" !!}">
                <a href="#">
                    <i class="fa fa-edit"></i>
                    <span>Course</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! request()->is('backend/course/create')?'active':"" !!}"><a href="{{ route('backend.course.create') }}"><i class="fa fa-circle-o"></i>Create</a></li>
                    <li class="{!! request()->is('backend/course')?'active':"" !!}"><a href="{{ route('backend.course') }}"><i class="fa fa-circle-o"></i>List</a></li>
                </ul>
            </li>

            {{--Contact Us--}}
            <li class="treeview {!! request()->is('backend/contactus*')?'active menu-open':"" !!}">
                <a href="#">
                    <i class="fa fa-edit"></i>
                    <span>Contact Us</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! request()->is('backend/contactus')?'active':"" !!}"><a href="{{ route('backend.contactus') }}"><i class="fa fa-circle-o"></i>List</a></li>
                </ul>
            </li>

            {{--News--}}
            <li class="treeview {!! request()->is('backend/news*')?'active menu-open':"" !!}">
                <a href="#">
                    <i class="fa fa-edit"></i>
                    <span>News</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! request()->is('backend/news/create')?'active':"" !!}"><a href="{{ route('backend.news.create') }}"><i class="fa fa-circle-o"></i>Create</a></li>
                    <li class="{!! request()->is('backend/news')?'active':"" !!}"><a href="{{ route('backend.news') }}"><i class="fa fa-circle-o"></i>List</a></li>
                </ul>
            </li>

            {{--Client--}}
            <li class="treeview {!! request()->is('backend/client*')?'active menu-open':"" !!}">
                <a href="#">
                    <i class="fa fa-edit"></i>
                    <span>Client</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! request()->is('backend/client/create')?'active':"" !!}"><a href="{{ route('backend.client.create') }}"><i class="fa fa-circle-o"></i>Create</a></li>
                    <li class="{!! request()->is('backend/client')?'active':"" !!}"><a href="{{ route('backend.client') }}"><i class="fa fa-circle-o"></i>List</a></li>
                </ul>
            </li>

            {{--Testimonial--}}
            <li class="treeview {!! request()->is('backend/testimonial*')?'active menu-open':"" !!}">
                <a href="#">
                    <i class="fa fa-edit"></i>
                    <span>Testimonial</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! request()->is('backend/testimonial/create')?'active':"" !!}"><a href="{{ route('backend.testimonial.create') }}"><i class="fa fa-circle-o"></i>Create</a></li>
                    <li class="{!! request()->is('backend/testimonial')?'active':"" !!}"><a href="{{ route('backend.testimonial') }}"><i class="fa fa-circle-o"></i>List</a></li>
                </ul>
            </li>

            <!-- settings -->
            <li>
                <a href="{{ route('backend.setting.edit', ['id' => 1]) }}"><i class="fa fa-bars"></i>Setting</a>
            </li>


        </ul>
    </section>
    <!-- /.sidebar -->
</aside>