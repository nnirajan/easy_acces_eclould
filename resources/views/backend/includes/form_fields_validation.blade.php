@if($errors->has($fieldname))
    <p class="text-red">{!! $errors->first($fieldname) !!}</p>
@endif