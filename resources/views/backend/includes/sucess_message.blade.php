@if(session()->has('message'))
    <section class="content-header fmessage">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Alert!</h4>
            {!! session()->get('message') !!}
        </div>
    </section>
    {{--@else--}}
    {{--<section class="content-header fmessage">--}}
    {{--<div class="alert alert-warning alert-dismissible">--}}
    {{--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>--}}
    {{--<h4><i class="icon fa fa-warning"></i> Alert!</h4>--}}
    {{--Sorry Data is not Inserted.--}}
    {{--</div>--}}
    {{--</section>--}}

@endif