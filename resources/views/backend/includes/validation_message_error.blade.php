@if ($errors->any())

    <div class="alert alert-warning customm alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
        Please fill out the form properly. Some fields are missing.
    </div>
    {{--<ul>--}}
        {{--@foreach ($errors->all() as $error)--}}
            {{--<li>{{ $error }}</li>--}}
        {{--@endforeach--}}
    {{--</ul>--}}
@endif