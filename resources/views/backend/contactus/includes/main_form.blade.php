 <div class="box-body">
    <div class="form-group">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name',null, ["class" => "form-control", "placeholder" => "Write Name"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'name'])
    </div>
    <div class="form-group">
        {!! Form::label('image', 'Image') !!}
        {!! Form::file('client_image',null, ["class" => "form-control"]) !!}
        @include('backend.includes.form_fields_validation',['fieldname' => 'client_image'])
    </div>
     <div class="form-group">
         {!! Form::label('rank', 'Rank') !!}
         {!! Form::number('rank',null, ["placeholder" => "Type URL", "class" => "form-control", "placeholder" => "Write Rank","min"=>1]) !!}
         @include('backend.includes.form_fields_validation',['fieldname' => 'rank'])
     </div>
</div>