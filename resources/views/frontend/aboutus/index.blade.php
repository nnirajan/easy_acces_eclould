@extends('frontend.layouts.master')

@section('content')
    @include('frontend.aboutus.includes.slider')

    <section class="section-entry">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="content content--background">
                    <h1 class="titlestyle1">{{$data['aboutus'][0]->name}}
                        <small></small>
                    </h1>
                    <p>{{$data['aboutus'][0]->short_description}}</p>
                    <p>{{$data['aboutus'][0]->description}}</p>
                </div>
            </div>
            {{--<div class="col-12">--}}
                {{--<div class="content padding40">--}}
                    {{--<h2 class="titlestyle1">Services--}}
                        {{--<small></small>--}}
                    {{--</h2>--}}
                    {{--<ul class="liststyle1">--}}
                        {{--@foreach($data['services'] as $service)--}}
                            {{--<a href="{{route('aboutus/show/'.$service->id)}}">--}}
                                {{--<li>--}}
                                {{--<span class="{{$service->icon}}"></span>--}}
                                {{--<div class="detail">--}}
                                    {{--<h2>{{$service->name}}</h2>--}}
                                    {{--<p>{{$service->description}}</p>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                            {{--</a>--}}
                        {{--@endforeach--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
</section>
@endsection