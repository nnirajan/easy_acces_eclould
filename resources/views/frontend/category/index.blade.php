@extends('frontend.layouts.master')

@section('seo')
    <title>{{ ucfirst(strtolower($data['cat_post'][0]->c_title)) }} : Article Category </title>
    <meta name="description" content="If you are looking to publish an article, do send your article to article@articlesearch.net">
    <meta name="keywords" content="A leading Article Portal in Nepal">

    <meta property="og:title" content="{{ ucfirst(strtolower($data['cat_post'][0]->c_title)) }} : Article Category ">
    <meta property="og:site_name" content="Article Search">
    <meta property="og:url" content="http://www.articlesearch.net/category/{{ $data['cat_post'][0]->slug }}">
    <meta property="og:description" content="If you are looking to publish an article, do send your article to article@articlesearch.net">
    <meta property="og:type" content="Web Site">


@endsection

@section('content')
    <section class="CategoryListCover">
        <div class="container">
            <div class="row">
                @include('frontend.category.includes.main_promotion')
                @include('frontend.category.includes.news_listing')
                @include('frontend.category.includes.sidebar')
            </div>
        </div>
    </section>
@endsection