<div class="col-lg-8 col-md-8 col-sm-8">
    <div class="nptitle">
        <h1>{{ $data['cat_post'][0]->c_title }}</h1>
    </div>
    <div class="categoryList">
        <ul>
            @if(isset($data['cat_post']) && $data['cat_post']->count()>0)
            @foreach($data['cat_post'] as $cp)
            <li class="clearfix">
                <div class="listImage">
                    <a href="{{route('article.details', $cp->slug)}}"><img src="{{ asset('images/post/936_525_'.$cp->image) }}" alt="{{ $cp->image_title }}" class="img-responsive lazy"></a>
                </div>
                    <div class="listText">
                    <h1><a href="{{route('article.details', $cp->slug)}}">{{ $cp->title }}</a></h1>
                    <p>{{ str_limit($cp->short_description),'400','...' }}</p>
                </div>
            </li>
                @endforeach
                {{ $data['cat_post']->links() }}
                @else
                    <li>Data not available</li>
                @endif




        </ul>
    </div>
</div>