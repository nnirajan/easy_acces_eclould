<div class="col-lg-4 col-md-4 col-sm-4">
    <div class="sidebar">
        <div class="nptitle">
            <h1>Latest News</h1>
        </div>
        <div class="latestNews">
            <ul>
                @foreach($data['sidebarNewsTop'] as $snp)
                <li>
                    <a href="{{route('article.details', $snp->slug)}}">{{ $snp->title }}</a>
                </li>
                @endforeach


                <li>
                    <div class="sidebarpromotion"> <img src="{{ asset('frontend/images/huffpost2.gif') }}" alt="" class="img-responsive lazy"> </div>
                </li>
                    @foreach($data['sidebarNewsBtn'] as $snb)
                <li>
                    <a href="{{route('article.details', $snb->slug)}}">{{ $snb->title }}</a>
                </li>
                    @endforeach

            </ul>
        </div>
        <div class="facebookPage">
            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Farticlesearch%2F&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=271862322867669" width="100%" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
        </div>
    </div>
</div>