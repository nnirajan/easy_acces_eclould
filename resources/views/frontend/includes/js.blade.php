<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{asset('/frontend/js/lib/slick.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/frontend/js/lib/jquery.slimmenu.js')}}"></script>
<script type="text/javascript" src="{{asset('/frontend/js/lib/jquery.fancybox.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/frontend/js/app.js')}}"></script>