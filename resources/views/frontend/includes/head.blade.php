<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="description" content="">
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <link rel="icon" type="image/icon" href="{{asset('images/favicon.ico')}}">
    <title> | Education Cloud</title>
    <link rel="stylesheet" href="{{asset('/frontend/css/app.css')}}">
</head>