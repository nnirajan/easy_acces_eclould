<div id="preloader-wrapper">
    <div class="preloader a" style="--n: 5;">
        <div class="dot" style="--i: 0;"></div>
        <div class="dot" style="--i: 1;"></div>
        <div class="dot" style="--i: 2;"></div>
        <div class="dot" style="--i: 3;"></div>
        <div class="dot" style="--i: 4;"></div>
    </div>
</div>
<header class="header">
    <div class="header-top">
        <div class="container">
            <div class="row flex flex-wrap flex-middle">
                <div class="col-12 col-xl-8">
                    <ul class="contact-detail">
                        <li><a href="tel:{{$settings[0]->phone}}" title="{{$settings[0]->phone}}"><span class="fa fa-phone"></span><span>{{$settings[0]->phone}}</span></a>
                        </li>
                        <li><a href="mailto:{{$settings[0]->email}}" title="{{$settings[0]->email}}"><span
                                        class="fa fa-envelope"></span><span>{{$settings[0]->email}}</span></a></li>
                        <li><span class="fa fa-map-marker"></span><span> {{$settings[0]->address}}</span></li>
                    </ul>
                </div>
                <div class="col-12 col-xl-4">
                    <ul class="social-media flex flex-end">
                        <li><a href="{{$settings[0]->facebook}}" target="_blank" class="fa fa-facebook"></a></li>
                        <li><a href="{{$settings[0]->twitter}}" target="_blank" class="fa fa-twitter"></a></li>
                        <li><a href="{{$settings[0]->youtube}}" target="_blank" class="fa fa-youtube"></a></li>
                        <li><a href="{{$settings[0]->googleplus}}" target="_blank" class="fa fa-google-plus"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row flex flex-middle paddingtb30">

            <figure class="col-12 col-md-3 logo-holder">
                <a href="/"><img src="{{asset('images/site-logo.png')}}" align="Yi Jiyan Construction"/></a>
            </figure>

            <nav class="col-12 col-md-9 flex flex-end flex-middle">
                <ul class="slimmenu">
{{--                    {{ Request::is('/') ? ' "active"' : '' }}--}}
                    <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="/">Home</a></li>
                    {{--{{ Request::is('about*') ? ' class="active"' : null }}--}}
                    <li class="{{ Request::is('aboutus') ? 'active' : '' }}"><a href="{{route($aboutus[0]->url)}}">About Us</a></li>
                    <li class="{{ Request::is('service') ? 'active' : '' }}"><a href="{{route('service')}}">Services</a></li>
                    <li class="{{ Request::is('course') ? 'active' : '' }}"><a href="{{route('course')}}">Courses</a></li>
                    <li class="{{ Request::is('news') ? 'active' : '' }}"><a href="{{route('news')}}">News</a></li>
                    <li class="{{ Request::is('contactus') ? 'active' : '' }}"><a href="{{route('contactus')}}">Contact Us</a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>
