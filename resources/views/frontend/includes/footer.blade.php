<footer class="footer bg-light">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-3">
                <h3>{{$aboutus[0]->name}}</h3>
                <p>{{$aboutus[0]->short_description}}
                    <a href="{{route('aboutus')}}" class="button ">Read More</a>
                </p>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <h3>Services</h3>
                <ul>
                    @foreach($services as $service)
                        <li><a href="{{route('service.show',$service->id)}}">{{$service->name}}</a></li>
                    @endforeach

                </ul>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <h3>Contact Us</h3>
                <ul>
                    {{--<li>1 Dowie Drive</li>--}}
                    <li>{{$settings[0]->address}}</li>
                    {{--<li>Australia</li>--}}
                    <li>Phone No : <a href="{{$settings[0]->phone}}" title="{{$settings[0]->phone}}">{{$settings[0]->phone}}</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <h3>Social</h3>
                <p>Join our social networks, get news and promotions</p>
                <ul class="social-media social-media--style2">
                    <li><a href="{{$settings[0]->facebook}}" target="_blank" class="fa fa-facebook"></a></li>
                    <li><a href="{{$settings[0]->twitter}}" target="_blank" class="fa fa-twitter"></a></li>
                    <li><a href="{{$settings[0]->youtube}}" target="_blank" class="fa fa-youtube"></a></li>
                    <li><a href="{{$settings[0]->googleplus}}" target="_blank" class="fa fa-google-plus"></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <span class="copyright">&copy; 2018 - EDUCATION CLOUD CONSULTANCY. All Rights Reserved. Designed By:</span>
                    <span class="architect">Designed By:
                        <a href="http://easyaccesstech.com.au" target="_blank">
                            <img src="{{asset('images/easyaccess-it-logo.png')}}" alt=""/>
                        </a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</footer>