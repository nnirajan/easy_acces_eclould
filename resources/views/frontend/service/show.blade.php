@extends('frontend.layouts.master')

@section('content')
    @include('frontend.service.includes.slider')

    <section class="section-container">
        <div class="container">
            <div class="row">
                <div class="col-12 paddingtop50 title-holder">
                    <h2 class="titlestyle1 titlestyle1--inline-block">Our Services
                        <small></small>
                    </h2>
                </div>
                <div class="col-12">
                    <div class="content content--background">
                        <h1 class="titlestyle1">
                            <span class="{{$data['service']->icon}}"></span>
                            {{$data['service']->name}}
                            <small></small>
                        </h1>
                        <p>{{$data['service']->description}}</p>
                        <p>Choosing to study abroad is a very important and life changing decision...</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection