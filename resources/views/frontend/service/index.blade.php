@extends('frontend.layouts.master')

@section('content')
    @include('frontend.service.includes.slider')

    <section class="section-container">
        <div class="container">
            <div class="row">
                <div class="col-12 paddingtop50 title-holder">
                    <h2 class="titlestyle1 titlestyle1--inline-block">Our Services
                        <small></small>
                    </h2>
                </div>
                <div class="col-12">
                    <div class="content padding40">
                        <ul class="liststyle1 liststyle1--3cols">
                            @foreach($data['services'] as $service)
                                <li>
                                    <span class="{{$service->icon}}"></span>
                                    <div class="detail">
                                        <a href="{{route('service.show',$service->id)}}"><h2>{{$service->name}}</h2></a>
                                        <p>{{$service->description}}</p>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection