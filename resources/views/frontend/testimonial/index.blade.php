@extends('frontend.layouts.master')

@section('content')
    <section class="testimonial-section paddingtb50 bg-image-secondary show-all">
        <div class="container">
            <div class="row">
                <div class="col-12 title-holder">
                    <h2 class="titlestyle1 titlestyle1--inline-block">Our Clients Say
                        <small></small>
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    @foreach($data['testimonials'] as $testimonial)
                        <div class="item">
                            <figure>
                                <img src="{{asset('images/testimonial/'.$testimonial->image)}}" alt=""/>
                            </figure>
                            <blockquote>{{$testimonial->testimonials}}</blockquote>
                            <span class="designation text-center">- {{$testimonial->name}}, {{$testimonial->office}}</span>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection