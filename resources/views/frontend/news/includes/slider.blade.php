<section class="section-billboard billboard">
    <div class="banner-holder">
        <figure>
            <img src="{{asset('/frontend/uploads/billboard/inner-banner-image.jpg')}}"  alt="">
        </figure>
    </div>
</section>
