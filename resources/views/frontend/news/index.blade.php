@extends('frontend.layouts.master')

@section('content')

    @include('frontend.news.includes.slider')

    <section class="section-list-basic paddingtb50">
    <div class="container">
        <div class="row">
            <div class="col-12 title-holder">
                <h2 class="titlestyle1 titlestyle1--inline-block">Our News
                    <small></small>
                </h2>
                <hr/>
            </div>
        </div>
        <div class="row">
            @foreach($data['news'] as $news)
                <div class="col-12 col-md-4">
                    <figure>
                        <img src="{{asset('images/news/'.$news->image)}}" width="350" height="284"  />
                    </figure>
                    <div class="content">
                        <h2>{{$news->title}}</h2>
                        <span class="post-meta">
                        </span>
                        <p>{{$news->short_description}}</p>
                        <a href="{{route('news.show',$news->id)}}" class="button button-outline">Read More</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>

@endsection