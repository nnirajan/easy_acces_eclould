@extends('frontend.layouts.master')

@section('content')
    <section class="section-container">
        <div class="container">
            <div class="row">
                <div class="col-12 paddingtop50 title-holder">
                    <h2 class="titlestyle1 titlestyle1--inline-block">Our News
                        <small></small>
                    </h2>
                </div>
                <div class="col-12">
                    <div class="col-12 col-md-12">
                        <figure>
                            <img src="{{asset('images/news/'.$data['news']->image)}}" width="350" height="284"  />
                        </figure>
                        <div class="content">
                            <h2>{{$data['news']->title}}</h2>
                            <span class="post-meta"></span>
                            <p>{{$data['news']->short_description}}</p>
                            {{--<a href="{{route('news.show',$news->id)}}" class="button button-outline">Read More</a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection