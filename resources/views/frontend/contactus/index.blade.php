@extends('frontend.layouts.master')

@section('content')
    @include('frontend.contactus.includes.slider')

<section class="section-contact paddingtb50">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="form-holder">
                    <h2>Contact Us</h2>
                    <strong style="color: green;">
                    </strong>
                    @include('frontend.includes.flash_message')
                    <form action="{{route('contactus.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @include('frontend.contactus.includes.main_form')
                        <div class="form-footer">
                            <input type="submit" name="btnSubmit" value="Send Now">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection