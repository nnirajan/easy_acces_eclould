<ul>
    <li>
        <label for="name">Full Name*</label>
        <input type="text" id="name" name="name" required="required" placeholder="">
    </li>
    <li>
        <label for="email">Email*</label>
        <input type="email" id="email" name="email" required="required" placeholder="">
    </li>
    <li>
        <label for="phone">Phone Number*</label>
        <input type="text" id="phone" name="phone" required="required" placeholder="">
    </li>
    <li class="fullwidth">
        <label for="message">Your Message</label>
        <textarea id="message" name="message" cols="30" rows="10"
                  placeholder=""></textarea>
    </li>
</ul>