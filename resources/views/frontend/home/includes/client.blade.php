<section class="section-container bg-light paddingtb50">
    <div class="container">
        <div class="row">
            <div class="col-12 title-holder">
                <h2 class="titlestyle1 titlestyle1--inline-block">Our Clients
                    <small></small>
                </h2>
            </div>
            <div class="col-12">
                <div class="content padding40 flex flex-middle flex-wrap flex-space-between">
                    @foreach($data['clients'] as $client)
                        <figure>
                            <img src="{{asset('images/client/'.$client->image)}}" width="190" height="90" alt="" />
                        </figure>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>