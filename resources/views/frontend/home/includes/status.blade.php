<section class="section-container bg-image paddingtb50">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <ul class="liststyle1">
                    <li>
                        <h2><small>{{$data['setting'][0]->clients}}</small>Our Clients</h2>
                    </li>
                    <li>
                        <h2><small>{{$data['setting'][0]->partners}}</small>Our Partners</h2>
                    </li>
                    <li>
                        <h2><small>{{$data['setting'][0]->branches}}</small>Our Branches</h2>
                    </li>
                    <li>
                        <h2><small>{{$data['setting'][0]->awards_winning}}</small>Awards Winning</h2>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-md-6">
                <div class="content">
                    <h1>{{$data['setting'][0]->why_choose_us_title}}</h1>
                    <p>{{$data['setting'][0]->why_choose_us_description}}</p>
                    <a href="{{route('service')}}" class="button">Services</a>
                </div>
            </div>
        </div>
    </div>
</section>
