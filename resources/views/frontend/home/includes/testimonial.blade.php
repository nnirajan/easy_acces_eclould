<section class="slider-section paddingtb50 bg-image-secondary viewport-animate">
    <div class="container">
        <div class="row">
            <div class="col-12 title-holder">
                <h2 class="titlestyle1 titlestyle1--inline-block">Our Clients Say
                    <small></small>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12 slider-main animate animate-down">
                @foreach($data['testimonials'] as $testimonial)
                    <div class="item">
                        <figure>
                            <img src="{{asset('images/testimonial/'.$testimonial->image)}}" alt=""/>
                        </figure>
                        <div class="text-center">
                            <br/>
                            <span class="fa fa-quote-left"></span>
                        </div>
                        <blockquote>{{$testimonial->testimonials}}</blockquote>
                        <span class="designation text-center">- {{$testimonial->name}}, {{$testimonial->position}}</span>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
