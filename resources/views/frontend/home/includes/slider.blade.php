<section class="section-billboard billboard">

<div class="content-slider">
    @foreach($data['sliders'] as $slider)
        <div class="item">
            <figure>
                <img src="{{asset('images/slider/'.$slider->image)}}" height="526px" width="1227px" >
            </figure>
            <div class="container overlay">
                <div class="row">
                    <div class="col-12">
                        <span class="fa fa-book-reader"></span>
                        <h2><span>{{$slider->name}}</span></h2>
                        <p>{{$slider->details}}</p>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

</div>
</section>