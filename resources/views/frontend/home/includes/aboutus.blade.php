<section class="section-entry">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-4">
                <div class="content content--background">
                    <h1 class="titlestyle1">{{$data['aboutus'][0]->name}}
                        <small></small>
                    </h1>
                    <p>{{$data['aboutus'][0]->short_description}}</p>
                </div>
            </div>
            <div class="col-12 col-lg-8">
                <div class="content padding40">
                    <h2 class="titlestyle1">Services
                        <small></small>
                    </h2>
                    <ul class="liststyle1">
                        @foreach($data['services'] as $service)

                                <li>
                                    <span class="{{$service->icon}}"></span>
                                    <div class="detail">
                                        <a href="{{route('service.show',$service->id)}}"><h2>{{$service->name}}</h2></a>
                                        <p>{{$service->description}}</p>
                                    </div>
                                </li>

                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>