@extends('frontend.layouts.master')

@section('content')
    @include('frontend.course.includes.slider')
    <section class="section-container">
        <div class="container">
            <div class="row">
                <div class="col-12 paddingtop50 title-holder">
                    <h2 class="titlestyle1 titlestyle1--inline-block">Our Courses
                        <small></small>
                    </h2>
                </div>
                <div class="col-12">
                    <div class="content padding40">
                        <ul class="liststyle1 liststyle1--3cols">
                            @foreach($data['courses'] as $course)
                                    <li>
                                        <div class="detail">
                                            <figure>
                                                <img src="{{asset('images/course/image/'.$course->image)}}" width="64" height="54" />
                                            </figure>
                                            <a href="{{route('course.show',$course->id)}}"><h2>{{$course->name}}</h2></a>
                                                <p>{{$course->description}}</p>
                                        </div>
                                    </li>
                                </a>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection