@extends('frontend.layouts.master')

@section('content')
    <section class="section-container">
        <div class="container">
            <div class="row">
                <div class="col-12 paddingtop50 title-holder">
                    <h2 class="titlestyle1 titlestyle1--inline-block">Our Course
                        <small></small>
                    </h2>
                </div>
                <div class="col-12">
                    <div class="content content--background">
                        <figure>
                            <img src="{{asset('images/course/image/'.$data['course']->image)}}" width="64" height="54" />
                        </figure>
                        <h1 class="titlestyle1">
{{--                            <span class="{{$data['service']->icon}}"></span>--}}
                            {{$data['course']->name}}
                            <small></small>
                        </h1>
                        <p>{{$data['course']->description}}</p>
                      </div>
                </div>
            </div>
        </div>
    </section>

@endsection