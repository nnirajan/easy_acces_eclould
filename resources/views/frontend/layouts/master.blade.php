<!-- default.html -->
<!DOCTYPE html>
<html lang="en">

@include('frontend.includes.head')

<body id="home">

@include('frontend.includes.header')

@yield('content')

@include('frontend.includes.footer')

@include('frontend.includes.js')
</body>
</html>