<!-- default.html -->
<!DOCTYPE html>
<html lang="en">

@include('frontend.includes.head')

<body id="home">

@include('frontend.includes.header')

@yield('section')
{{--<section class="section-entry">--}}
    {{--<div class="container">--}}
        {{--<div class="row">--}}
            {{--<div class="col-12">--}}
                {{--<div class="content content--background">--}}
                    {{--<h1 class="titlestyle1">About Us--}}
                        {{--<small></small>--}}
                    {{--</h1>--}}
                    {{--<p>Education cloud is an innovative and experienced organization, it's aims to provide informed and--}}
                        {{--caring counselling to assist International students in making the best choices to achieve their--}}
                        {{--educational, immigration and and service living in Australia.</p>--}}
                    {{--<p>Choosing to study abroad is a very important and life changing decision...</p>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-12">--}}
                {{--<div class="content padding40">--}}
                    {{--<h2 class="titlestyle1">Services--}}
                        {{--<small></small>--}}
                    {{--</h2>--}}
                    {{--<ul class="liststyle1">--}}
                        {{--<li>--}}
                            {{--<span class="fa fa-graduation-cap"></span>--}}
                            {{--<div class="detail">--}}
                                {{--<h2>Education Service</h2>--}}
                                {{--<p>Our expert educational counselors guide the students from helping them choose--}}
                                    {{--institutions...</p>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<span class="fa fa-plane"></span>--}}
                            {{--<div class="detail">--}}
                                {{--<h2>Migration Services</h2>--}}
                                {{--<p>With years of experience in a whole range of immigration cases and scenarios, we have--}}
                                    {{--helped thousan...</p>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<span class="fa fa-book"></span>--}}
                            {{--<div class="detail">--}}
                                {{--<h2>Professional Year Program</h2>--}}
                                {{--<p>Make a smooth transition from educational to professional life...</p>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<span class="fa fa-trophy"></span>--}}
                            {{--<div class="detail">--}}
                                {{--<h2>Internship & Career Management</h2>--}}
                                {{--<p>We help our students develop a viable career plan that will serve them and their--}}
                                    {{--employers...</p>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</section>--}}

@include('frontend.includes.footer')

@include('frontend.includes.js')
</body>
</html>